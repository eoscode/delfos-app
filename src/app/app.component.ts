import {Component, OnInit} from '@angular/core';
import {AuthService} from './core/services/auth.service';
import {ErrorService} from './core/services/error.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import {take} from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'delfos-webapp';

  constructor(
    private authService: AuthService,
    private errorService: ErrorService,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.authService.autoLogin()
      .pipe(
        take(1)
      ).subscribe(
      null,
      error => {
        const message = this.errorService.getErrorMessage('Token inválido! Autenticação necessária.');
        this.snackBar.open(
          `Erro: ${message}`,
          'Ok',
          {duration: 5000, verticalPosition: 'top'}
        );
      }
    );
  }

}
