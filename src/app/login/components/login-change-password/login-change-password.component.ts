import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import {AuthService} from '../../../core/services/auth.service';
import {UserService} from '../../../user/services/user.service';
import {ErrorService} from '../../../core/services/error.service';
import {Observable} from 'rxjs';
import {catchError, finalize, tap} from 'rxjs/operators';
import {User} from '../../../core/models/user.model';

@Component({
  selector: 'app-login-change-password',
  templateUrl: './login-change-password.component.html',
  styleUrls: ['./login-change-password.component.scss']
})
export class LoginChangePasswordComponent implements OnInit {

  changePasswordForm: FormGroup;

  configs = {
    inLoading: false,
    isSaving: false,
    hidePassword: true,
    confirmPassword: false
  };

  constructor(
    public dialogRef: MatDialogRef<LoginChangePasswordComponent>,
    @Inject(MAT_DIALOG_DATA) public data: LoginChangePasswordData,
    private snackBar: MatSnackBar,
    private authService: AuthService,
    private userService: UserService,
    private errorService: ErrorService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit() {

    this.dialogRef.disableClose = true;
    this.createForm();

  }

  private createForm(): void {

    this.changePasswordForm = this.formBuilder.group({
      newPassword: ['', Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(10)])],
      confirm_newPassword: ['', [Validators.required]]
    });

    if (!this.data.usuario) {
      this.changePasswordForm.addControl('password',
        new FormControl('', [Validators.required]));
    }

  }

  onValidatePassword(): void {

    const passwordControl = this.changePasswordForm.get('newPassword');
    const confirmPasswordControl = this.changePasswordForm.get('confirm_newPassword');

    if (passwordControl == null || confirmPasswordControl == null) {
      this.configs.confirmPassword = true;
      return;
    }

    this.configs.confirmPassword = (passwordControl.value === confirmPasswordControl.value);

    const control = this.changePasswordForm.get('confirm_newPassword');
    if (this.configs.confirmPassword) {
      control.setErrors(null);
    } else {
      control.setErrors({'confirm_password': true});
    }

  }

  onSubimit(): void {

    this.onValidatePassword();
    if (!this.configs.confirmPassword) {
      return;
    }

    this.configs.isSaving = true;
    const changePassword = this.changePasswordForm.value;

    let operation: Observable<any>;
    if (this.data.usuario) {
      operation = this.changePassword(this.data.usuario, changePassword.newPassword);
    } else {
      operation = this.changeMyPassword(changePassword.password, changePassword.newPassword);
    }

    operation.pipe(
      tap(() => {
        this.snackBar.open(`Alteração realizada com sucesso.`,
          'OK', {verticalPosition: 'top', duration: 5000});
        this.dialogRef.close({successful: true});
      }),
      catchError((error) => {
        return this.errorService.handleError(error);
      }),
      finalize(() => {
        this.configs.isSaving = false;
        this.configs.inLoading = false;
      })
    ).subscribe();

  }

  onNoClick(): void {
    this.dialogRef.close({});
  }

  private changeMyPassword(password: string, newPassword: string): Observable<any> {
    return this.authService.changePasswordCurrentUser(password, newPassword);
  }

  private changePassword(usuario: User, password: string): Observable<any> {
    return this.userService.changePassword(usuario, password);
  }

}

export class LoginChangePasswordData {
  title?: string;
  usuario?: User;
}

