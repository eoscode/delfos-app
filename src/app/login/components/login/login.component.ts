import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import {AuthService} from '../../../core/services/auth.service';
import { ErrorService } from '../../../core/services/error.service';

import { Observable, of } from 'rxjs';
import { takeWhile, catchError, tap } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material/snack-bar';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {

  loginForm: FormGroup;
  configs = {
    inLoading: false,
    hidePassword: true
  };
  private alive = true;

  constructor(
    public authService: AuthService,
    private errorService: ErrorService,
    private formBuilder: FormBuilder,
    private snackBar: MatSnackBar,
    private router: Router
  ) { }

  ngOnInit() {
    this.createForm();
  }

  ngOnDestroy() {
    this.alive = false;
  }

  createForm(): void {
    this.loginForm = this.formBuilder.group({
      identifier: ['', [Validators.required]],
      password: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(8)]]
    });
  }

  onSubimit(): void {

    this.configs.inLoading = true;
    const operation: Observable<any> = this.authService.login(this.loginForm.value);

    operation
      .pipe(
        takeWhile(() => this.alive),
        tap((res) => {
          const redirectUrl: string = this.authService.redirectUrl || '/dashboard';

          this.router.navigate([redirectUrl]);
          this.authService.redirectUrl = null;
          this.configs.inLoading = false;
        }),
        catchError((error) => {
          console.log(error);
          this.configs.inLoading = false;
          this.snackBar.open(`Erro de autenticação ${error.status} - ${error.statusText || ''}`,
            'OK', {verticalPosition: 'top', duration: 5000});
          return of();
        })
      )
      .subscribe();
  }

  get identifier(): FormControl {
    return <FormControl>this.loginForm.get('identifier');
  }

  get password(): FormControl {
    return <FormControl>this.loginForm.get('password');
  }

  onKeepLogin(): void {
    this.authService.toggleKeepLogin();
  }

}
