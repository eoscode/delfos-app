import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SummaryItemCardComponent } from './summary-item-card.component';

describe('SummaryItemCardComponent', () => {
  let component: SummaryItemCardComponent;
  let fixture: ComponentFixture<SummaryItemCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SummaryItemCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SummaryItemCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
