import {Component, Input, OnInit,} from '@angular/core';

@Component({
  selector: 'app-summary-item-card',
  templateUrl: './summary-item-card.component.html',
  styleUrls: ['./summary-item-card.component.scss']
})
export class SummaryItemCardComponent implements OnInit {

  @Input() title: string;
  @Input() count: number;
  @Input() icon: string;

  constructor() { }

  ngOnInit() {
  }

}
