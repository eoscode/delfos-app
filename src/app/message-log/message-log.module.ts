import {NgModule} from '@angular/core';

import {MessageLogRoutingModule} from './message-log-routing.module';
import {SharedModule} from '../shared/shared.module';
import { MessageLogListComponent } from './components/message-log-list/message-log-list.component';
import {MessageModule} from '../message/message.module';
import {MessageSearchComponent} from '../message/components/message-search/message-search.component';
import {DialogConfirmComponent} from '../shared/components/dialog-confirm/dialog-confirm.component';
import { DataFieldLogSearchComponent } from './components/datafield-log-search/data-field-log-search.component';
import { MessageLogDetailComponent } from './components/message-log-detail/message-log-detail.component';

@NgModule({
  declarations: [MessageLogListComponent, DataFieldLogSearchComponent, MessageLogDetailComponent],
  imports: [
    SharedModule,
    MessageLogRoutingModule,
    MessageModule
  ],
  exports: [DataFieldLogSearchComponent],
  entryComponents: [
    MessageSearchComponent, DialogConfirmComponent, DataFieldLogSearchComponent, MessageLogDetailComponent
  ]
})
export class MessageLogModule { }
