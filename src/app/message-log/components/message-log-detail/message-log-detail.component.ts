import {Component, Inject, OnInit, Optional} from '@angular/core';
import {FormBuilder, FormControl} from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import {ErrorService} from '../../../core/services/error.service';
import {catchError, finalize, tap} from 'rxjs/operators';
import {MessageLog} from '../../../core/models/message-log.model';
import {MessageLogService} from '../../services/message-log.service';
import {DataFieldLog} from '../../../core/models/data-field-log';

@Component({
  selector: 'app-message-log-detail',
  templateUrl: './message-log-detail.component.html',
  styleUrls: ['./message-log-detail.component.scss']
})
export class MessageLogDetailComponent implements OnInit {

  messageLog: MessageLog = {
    dataFields: []
  };

  displayedColumns = ['name', 'value'];
  dataSource = new MatTableDataSource<DataFieldLog>();

  filter = new FormControl();
  filteredValues = { name: ''};

  configs = {
    inLoading: false,
    inSaving: false
  };

  constructor(
    @Optional() public dialogRef: MatDialogRef<MessageLogDetailComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: MessageLogDetailData,
    private formBuilder: FormBuilder,
    private messageLogService: MessageLogService,
    private errorService: ErrorService,
  ) { }

  ngOnInit() {
    if (this.dialogRef) {
      this.dialogRef.disableClose = true;
    }

    this.filter.valueChanges
      .subscribe(value => {
        this.filteredValues['name'] = value;
        this.dataSource.filter = JSON.stringify(this.filteredValues);
      });

    this.dataSource.filterPredicate = this.createFilter();

    if (this.data.messageLog) {
      this.messageLog = this.data.messageLog;

      this.configs.inLoading = true;
      this.messageLogService.findById(this.messageLog.id)
        .pipe(
          tap((data) => {
            this.messageLog = data;
            this.dataSource.data = data.dataFields;
          }),
          catchError((error) => {
            return this.errorService.handleError(error);
          }),
          finalize(() => {
            this.configs.inLoading = false;
          })
        ).subscribe();

      this.dataSource.data = this.messageLog.dataFields;
    }

  }

  private createFilter() {

    const filterFunction = function(data: DataFieldLog, filter): boolean {

      const searchTerms = JSON.parse(filter);

      return data.name.indexOf(searchTerms.name) !== -1;


    };
    return filterFunction;
  }

  onSubmit(): void {
    this.dialogRef.close({});
  }

}


export class MessageLogDetailData {
  messageLog: MessageLog;
}
