import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MessageLogDetailComponent } from './message-log-detail.component';

describe('MessageLogDetailComponent', () => {
  let component: MessageLogDetailComponent;
  let fixture: ComponentFixture<MessageLogDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MessageLogDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessageLogDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
