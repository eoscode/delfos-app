import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {HttpDataSource} from '../../../shared/components/http-data-source/http-data.source';
import { MatAutocomplete, MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MatChipInputEvent } from '@angular/material/chips';
import { MatDialog } from '@angular/material/dialog';
import { MatExpansionPanel } from '@angular/material/expansion';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import {ErrorService} from '../../../core/services/error.service';
import {catchError, finalize, map, startWith, tap} from 'rxjs/operators';
import {MessageLog} from '../../../core/models/message-log.model';
import {MessageLogService} from '../../services/message-log.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MessageSearchComponent} from '../../../message/components/message-search/message-search.component';
import {ComponentService} from '../../../component/services/component.service';

import {Component as ComponentModel} from '../../../core/models/component.model';
import {Message} from '../../../core/models/message.model';
import {DataFieldLogSearchComponent, DataFieldLogSearchData} from '../datafield-log-search/data-field-log-search.component';
import {Observable, ReplaySubject} from 'rxjs';
import {MessageService} from '../../../message/services/message.service';
import {DataFieldLog} from '../../../core/models/data-field-log';
import {ConditionUtil} from '../../../core/utils/condition-util';
import {DialogConfirmComponent, DialogConfirmData} from '../../../shared/components/dialog-confirm/dialog-confirm.component';
import {MessageLogDetailComponent, MessageLogDetailData} from '../message-log-detail/message-log-detail.component';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {MessageLogStatisticView} from '../../../core/models/view/message-log-statistic-view.model';
import {FilterMessageLogStatistic} from '../../../core/models/filters/filter-message-log-statistic';

@Component({
  selector: 'app-message-log-list',
  templateUrl: './message-log-list.component.html',
  styleUrls: ['./message-log-list.component.scss']
})
export class MessageLogListComponent implements OnInit {

  @ViewChild('filterPanel', { static: true }) filterPanel: MatExpansionPanel;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  @ViewChild('groupByInput', { static: false }) groupByInput: ElementRef<HTMLInputElement>;
  @ViewChild('auto', { static: false }) matAutocomplete: MatAutocomplete;

  displayedColumns = ['message', 'dateHour', 'component', 'identifier', 'actions'];
  dataSource = new HttpDataSource<MessageLog>();
  components: ComponentModel[] = [];
  dataFields: DataFieldLog[] = [];

  displayedColumnsGroup = [];
  dataSourceGroup = new HttpDataSource<MessageLogStatisticView>();

  selectable = true;
  removable = true;

  configs = {
    resultsLength: 0,
    inLoadingResults: false,
    isRateLimitReached: false,
    pageable: {
      pageIndex: 0,
      pageSize: 20,
      length: 0
    },
    search: {
      done: false,
      message: null,
      selectable: true,
      removable: true
    }
  };

  searchForm: FormGroup;
  statisticCtrl: FormControl = new FormControl();

  groupConfig: any = {
    enabled: false,
    addOnBlur: true,
    separatorKeysCodes: [ENTER, COMMA],
    filteredGroups: Observable,
    selected: [] = [],
    all: [] = ['Data', 'Data e Hora', 'Componente', 'Identificador', 'Mensagem'],
    allValues: [] = ['date', 'dateHour', 'component', 'identifier', 'message'],
  };

  private messageSubject = new ReplaySubject<string>(1);

  constructor(
    private messageService: MessageService,
    private messageLogService: MessageLogService,
    private componentService: ComponentService,
    private errorService: ErrorService,
    private formBuilder: FormBuilder,
    public dialog: MatDialog
  ) { }

  ngOnInit() {

    this.configs.inLoadingResults = true;
    this.dataSource.paginator = this.paginator;

    this.groupConfig.filteredGroups = this.statisticCtrl.valueChanges.pipe(
      startWith(null),
      map((group: string | null) => group ? this._filter(group) : this.groupConfig.all.slice()));

    this.paginator.page.subscribe((event: PageEvent) => {
      this.load();
    });

    this.createSearchForm();

    this.load();
    this.loadComponents();

    this.dataSource.filterPredicate =
      (data: MessageLog, filter: string) => {
        return data.message.toLowerCase().indexOf(filter) !== -1
          || data.identifier.toLowerCase().indexOf(filter) !== -1;
      };

    this.messageSubject.subscribe((messageId => {
      this.configs.inLoadingResults = true;
      this.messageService.findDetailById(messageId)
        .pipe(
          tap((result: Message) => {
            this.configs.search.message = result;
          }),
          catchError(this.errorService.handleError),
          finalize(() => {
            this.configs.inLoadingResults = false;
          })
        ).subscribe();
    }));

  }

  private load(search?: boolean): void {

    if (search) {
      this.paginator.pageIndex = 0;
    }

    this.configs.inLoadingResults = true;
    const modelSearch: MessageLog = {
      ...this.searchForm.value,
      dataFields: this.dataFields
    };

    this.messageLogService.find(modelSearch)
      .pipe(
        map(res => {
          this.configs.pageable.length = res.total;

          if (res.pageable) {
            this.configs.pageable.pageIndex = res.pageable.page;
          }

          return res.content || [];
        }),
        tap((data) => {
          this.configs.resultsLength = data.length;
          this.dataSource.updateData(data, this.configs.pageable.length);

          if (search && data.length > 0) {
            this.filterPanel.close();
            this.configs.search.done = true;
          }

        }),
        catchError((error) => {
          return this.errorService.handleError(error);
        }),
        finalize(() => {
          this.configs.inLoadingResults = false;
        })
      ).subscribe();

  }

  private loadComponents(): void {
    this.componentService.findAll()
      .pipe(
        tap((data) => {
          this.components = data;
          if (this.components.length > 0) {
            this.searchForm.get('componentId').enable();
          }
        }),
        catchError(error => {
          return this.errorService.handleError(error);
        })
      ).subscribe();
  }

  private createSearchForm(): void {
    this.searchForm = this.formBuilder.group({
      messageId: [null],
      dateHour: [new Date(), [Validators.required]],
      dateHourEnd: [{value: null, disabled: true}],
      componentId: [{value: null, disabled: true}],
      identifier: [null]
    });
  }

  messageSearch(): void {
    const dialogRef = this.dialog.open(MessageSearchComponent);

    dialogRef.beforeClosed()
      .subscribe((result => {
        if (result.data) {
         this.messageReset();
         this.configs.search.message = result.data;
         this.messageSubject.next(result.data.id);
         this.searchForm.patchValue({messageId: result.data.id});
        }
      }));
  }

  messageReset(): void {
    this.configs.search.message = null;
    this.dataFields = [];
    this.searchForm.patchValue({messageId: null});
  }

  search(): void {
    this.load(true);
  }

  dataFieldSearch(): void {
    const dialogRef = this.dialog.open<DataFieldLogSearchComponent, DataFieldLogSearchData>(
      DataFieldLogSearchComponent,
      {
        data: {message: this.configs.search.message}
      });

    dialogRef.beforeClosed()
      .subscribe((dataField: DataFieldLog) => {
        if (dataField) {

          const find = this.dataFields.find(data => {
            return (data.fieldId === dataField.fieldId)
              && (data.operator === dataField.operator)
              && (data.value === dataField.value);
          });

          if (find) {
            this.dialog.open<DialogConfirmComponent, DialogConfirmData, boolean>(
              DialogConfirmComponent,
              { data: {
                  title: 'Atenção',
                  message: `Filtro '${this.getExpression(dataField)}' já adicionado.`,
                  cancelButton: false,
                  primaryButton: 'OK'
                } }
            );
          } else {
            this.dataFields.push(dataField);
          }

        }
      });
  }

  editDataFieldSearch(dataFieldLog: DataFieldLog): void {
    const idx = this.dataFields.indexOf(dataFieldLog);
    const dialogRef = this.dialog.open<DataFieldLogSearchComponent, DataFieldLogSearchData>(
      DataFieldLogSearchComponent,
      {
        data: {
          message: this.configs.search.message,
          dataField: dataFieldLog
        }
      });

    dialogRef.beforeClosed()
      .subscribe((dataField: DataFieldLog) => {
        if (dataField) {
          this.dataFields[idx] = dataField;
        }
      });
  }

  removeDataFieldFilter(dataField: DataFieldLog): void {
    this.dataFields.splice(this.dataFields.indexOf(dataField), 1);
  }

  reset(): void {
    this.searchForm.reset();
    this.configs.search.message = null;
    this.dataFields = [];
    this.groupConfig.selectGroup = [];
    this.groupConfig.enabled = false;
    this.configs.search.done = false;
    this.load();
  }

  changeDate(input): void {
    if (input.value) {
      this.searchForm.get('dateHourEnd').enable();
    } else {
      const formControl = this.searchForm.get('dateHourEnd');
      formControl.reset();
      formControl.disable();
    }
  }

  changeDataHour(value): void {
    if (value !== undefined) {
      this.searchForm.get('dataHourEnd').enable();
    } else {
      const formControl = this.searchForm.get('dataHourEnd');
      formControl.reset();
      formControl.disable();
    }
  }

  refresh(): void {
    this.load();
  }

  getExpression(dataFieldLog: DataFieldLog): string {
    return ConditionUtil.getDataFieldLogExpression(dataFieldLog);
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  view(messageLog: MessageLog): void {
    this.dialog.open<MessageLogDetailComponent, MessageLogDetailData>(MessageLogDetailComponent,
      {
        data: {
          messageLog: messageLog
        }
      });
  }

  statistic(): void {

    const groupValues: string[] = [];
    this.displayedColumnsGroup = [];

    this.groupConfig.selected.forEach((group) => {
      const idx = this.groupConfig.all.findIndex(item => item === group);
      if (idx >= 0) {
        groupValues.push(this.groupConfig.allValues[idx]);
      }
    });
    this.displayedColumnsGroup = [...groupValues];
    this.displayedColumnsGroup.push('count');

    const filterBy: MessageLog = {
      ...this.searchForm.value,
      dataFields: this.dataFields
    };

    const statisticBy: FilterMessageLogStatistic = {
      filters: {...filterBy},
      groupBy: groupValues
    };

    this.configs.inLoadingResults = true;
    this.messageLogService.statistic(statisticBy)
      .pipe(
        tap((data) => {
          this.dataSourceGroup.updateData(data, data.length);
        }),
        catchError((error) => {
          return this.errorService.handleError(error);
        }),
        finalize(() => {
          this.configs.inLoadingResults = false;
        })
      ).subscribe();
  }

  addGroup(event: MatChipInputEvent): void {

    if (!this.matAutocomplete.isOpen) {
      const input = event.input;
      const value = event.value;

      if ((value || '').trim()) {
        if (!this.groupConfig.all.find((item) => item === value)) {
          return;
        }
        this.groupConfig.selected.push(value.trim());
      }

      // Reset the input value
      if (input) {
        input.value = '';
      }

      this.statisticCtrl.setValue(null);
    }
  }

  removeGroup(group: string): void {
    const index = this.groupConfig.selected.indexOf(group);

    if (index >= 0) {
      this.groupConfig.selected.splice(index, 1);
    }
  }

  removeAllGroups(): void {
    this.groupConfig.selected = [];
    this.dataSourceGroup.data = [];
    this.displayedColumnsGroup = [];
  }

  selectGroup(event: MatAutocompleteSelectedEvent): void {
    this.groupConfig.selected.push(event.option.viewValue);
    this.groupByInput.nativeElement.value = '';
    this.statisticCtrl.setValue(null);
  }

  isColumnGroup(column: string) {
    return this.displayedColumnsGroup.find((item) => item === column);
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.groupConfig.all.filter(group => group.toLowerCase().indexOf(filterValue) === 0);
  }

}
