import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {MessageLogListComponent} from './components/message-log-list/message-log-list.component';
import {AuthGuard} from '../login/auth.guard';

const routes: Routes = [
  {
    path: '',
    component: MessageLogListComponent,
    canActivate: [ AuthGuard ],
    canActivateChild: [ AuthGuard ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MessageLogRoutingModule { }
