import {NgModule} from '@angular/core';

import {EventLogRoutingModule} from './event-log-routing.module';
import {SharedModule} from '../shared/shared.module';
import {EventLogListComponent} from './components/event-log-list/event-log-list.component';
import {MessageModule} from '../message/message.module';
import {RuleModule} from '../rule/rule.module';
import {MessageSearchComponent} from '../message/components/message-search/message-search.component';
import {RuleSearchComponent} from '../rule/components/rule-search/rule-search.component';
import {EventModule} from '../event/event.module';
import {EventSearchComponent} from '../event/components/event-search/event-search.component';
import { EventLogDetailComponent } from './components/event-log-detail/event-log-detail.component';
import {MessageLogModule} from '../message-log/message-log.module';
import {DataFieldLogSearchComponent} from '../message-log/components/datafield-log-search/data-field-log-search.component';
import {EventDetailComponent} from '../event/components/event-detail/event-detail.component';


@NgModule({
  declarations: [EventLogListComponent, EventLogDetailComponent],
  imports: [
    SharedModule,
    EventLogRoutingModule,
    EventModule,
    MessageModule,
    RuleModule,
    MessageLogModule
  ],
  entryComponents: [
    EventSearchComponent, MessageSearchComponent, RuleSearchComponent, EventLogDetailComponent, DataFieldLogSearchComponent,
    EventDetailComponent
  ]
})
export class EventLogModule { }
