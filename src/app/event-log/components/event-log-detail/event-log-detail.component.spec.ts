import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventLogDetailComponent } from './event-log-detail.component';

describe('EventLogDetailComponent', () => {
  let component: EventLogDetailComponent;
  let fixture: ComponentFixture<EventLogDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventLogDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventLogDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
