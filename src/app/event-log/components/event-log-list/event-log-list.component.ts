import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {EventLog} from '../../../core/models/event-log.model';
import {HttpDataSource} from '../../../shared/components/http-data-source/http-data.source';
import {MessageService} from '../../../message/services/message.service';
import {ComponentService} from '../../../component/services/component.service';
import {ErrorService} from '../../../core/services/error.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import { MatAutocomplete, MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MatChipInputEvent } from '@angular/material/chips';
import { MatDialog } from '@angular/material/dialog';
import { MatExpansionPanel } from '@angular/material/expansion';
import { MatPaginator } from '@angular/material/paginator';
import {Observable, ReplaySubject} from 'rxjs';
import {catchError, finalize, map, startWith, tap} from 'rxjs/operators';
import {MessageSearchComponent} from '../../../message/components/message-search/message-search.component';
import {Component as ComponentModel} from '../../../core/models/component.model';
import {RuleSearchComponent} from '../../../rule/components/rule-search/rule-search.component';
import {EventLogService} from '../../services/event-log.service';
import {EventSearchComponent} from '../../../event/components/event-search/event-search.component';
import {EventLogDetailComponent, EventLogDetailData} from '../event-log-detail/event-log-detail.component';
import {
  DataFieldLogSearchComponent,
  DataFieldLogSearchData
} from '../../../message-log/components/datafield-log-search/data-field-log-search.component';
import {DataFieldLog} from '../../../core/models/data-field-log';
import {DialogConfirmComponent, DialogConfirmData} from '../../../shared/components/dialog-confirm/dialog-confirm.component';
import {ConditionUtil} from '../../../core/utils/condition-util';
import {EventDetailComponent, EventDetailData} from '../../../event/components/event-detail/event-detail.component';
import {RuleDetailComponent, RuleDetailData} from '../../../rule/components/rule-detail/rule-detail.component';
import {MessageDetailComponent, MessageDetailData} from '../../../message/components/message-detail/message-detail.component';
import {EventType} from '../../../core/models/event.model';
import {EventUtil} from '../../../core/utils/event-util';
import {FilterEventLogStatistic} from '../../../core/models/filters/filter-event-log-statistic';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {EventLogStatisticView} from '../../../core/models/view/event-log-statistic-view.model';

@Component({
  selector: 'app-event-log',
  templateUrl: './event-log-list.component.html',
  styleUrls: ['./event-log-list.component.scss']
})
export class EventLogListComponent implements OnInit {

  @ViewChild('filterPanel', { static: true }) filterPanel: MatExpansionPanel;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  @ViewChild('groupByInput', { static: false }) groupByInput: ElementRef<HTMLInputElement>;
  @ViewChild('auto', { static: false }) matAutocomplete: MatAutocomplete;

  displayedColumns = ['occurrence', 'event', 'dateHour', 'rule', 'message', 'actions'];
  dataSource = new HttpDataSource<EventLog>();
  components: ComponentModel[] = [];
  dataFields: DataFieldLog[] = [];

  displayedColumnsGroup = [];
  dataSourceGroup = new HttpDataSource<EventLogStatisticView>();

  selectable = true;
  removable = true;

  configs = {
    resultsLength: 0,
    inLoadingResults: false,
    isRateLimitReached: false,
    pageable: {
      pageIndex: 0,
      pageSize: 20,
      length: 0
    },
    search: {
      done: false,
      event: null,
      rule: null,
      message: null,
      selectable: true,
      removable: true
    }
  };

  searchForm: FormGroup;
  statisticCtrl: FormControl = new FormControl();

  groupConfig: any = {
    enabled: false,
    addOnBlur: true,
    separatorKeysCodes: [ENTER, COMMA],
    filteredGroups: Observable,
    selected: [] = [],
    all: [] = ['Data', 'Data e Hora', 'Componente', 'Identificador', 'Evento', 'Regra', 'Mensagem'],
    allValues: [] = ['date', 'dateHour', 'component', 'identifier', 'event', 'rule', 'message'],
  };

  private messageSubject = new ReplaySubject<string>(1);

  constructor(
    private messageService: MessageService,
    private eventLogService: EventLogService,
    private componentService: ComponentService,
    private errorService: ErrorService,
    private formBuilder: FormBuilder,
    public dialog: MatDialog
  ) { }

  ngOnInit() {

    this.configs.inLoadingResults = true;
    this.dataSource.paginator = this.paginator;

    this.groupConfig.filteredGroups = this.statisticCtrl.valueChanges.pipe(
      startWith(null),
      map((group: string | null) => group ? this._filter(group) : this.groupConfig.all.slice()));

    this.paginator.page.subscribe(() => {
      this.load();
    });

    this.createSearchForm();

    this.load();
    this.loadComponents();

    this.dataSource.filterPredicate =
      (data: EventLog, filter: string) => {
        return data.message.name.toLowerCase().indexOf(filter) !== -1
          || data.event.name.toLowerCase().indexOf(filter) !== -1
          || data.rule.name.toLowerCase().indexOf(filter) !== -1;
      };

    this.messageSubject.subscribe((messageId => {
      this.configs.inLoadingResults = true;
      this.messageService.findDetailById(messageId)
        .pipe(
          tap((result: EventLog) => {
            this.configs.search.message = result;
          }),
          catchError(this.errorService.handleError),
          finalize(() => {
            this.configs.inLoadingResults = false;
          })
        ).subscribe();
    }));

  }

  private load(search?: boolean): void {

    if (search) {
      this.paginator.pageIndex = 0;
    }

    this.configs.inLoadingResults = true;
    const modelSearch: EventLog = {
      ...this.searchForm.value,
      dataFields: this.dataFields
    };

    this.eventLogService.find(modelSearch, this.paginator.pageSize,
      this.paginator.pageIndex)
      .pipe(
        map(res => {
          this.configs.pageable.length = res.total;

          if (res.pageable) {
            this.configs.pageable.pageIndex = res.pageable.page;
          }

          return res.content || [];
        }),
        tap((data) => {
          this.configs.resultsLength = data.length;
          this.dataSource.updateData(data, this.configs.pageable.length);

          if (search && data.length > 0) {
            this.filterPanel.close();
            this.configs.search.done = true;
          }

        }),
        catchError((error) => {
          return this.errorService.handleError(error);
        }),
        finalize(() => {
          this.configs.inLoadingResults = false;
        })
      ).subscribe();

  }

  private createSearchForm(): void {
    this.searchForm = this.formBuilder.group({
      messageId: [null],
      dateHour: [new Date(), [Validators.required]],
      dateHourEnd: [{value: null, disabled: true}],
      eventId: [null],
      ruleId: [null],
      componentId: [{value: null, disabled: true}],
      identifier: [null]
    });
  }

  private loadComponents(): void {
    this.componentService.findAll()
      .pipe(
        tap((data) => {
          this.components = data;
          if (this.components.length > 0) {
            this.searchForm.get('componentId').enable();
          }
        }),
        catchError(error => {
          return this.errorService.handleError(error);
        })
      ).subscribe();
  }

  eventSearch(): void {
    const dialogRef = this.dialog.open(EventSearchComponent);

    dialogRef.beforeClosed()
      .subscribe((result => {
        if (result.data) {
          this.eventReset();
          this.configs.search.event = result.data;
          this.searchForm.patchValue({eventId: result.data.id});
        }
      }));
  }

  eventReset(): void {
    this.configs.search.event = null;
    this.searchForm.patchValue({eventId: null});
  }

  ruleSearch(): void {
    const dialogRef = this.dialog.open(RuleSearchComponent);

    dialogRef.beforeClosed()
      .subscribe((result => {
        if (result.data) {
          this.ruleReset();
          this.configs.search.rule = result.data;
          this.searchForm.patchValue({ruleId: result.data.id});
        }
      }));
  }

  ruleReset(): void {
    this.configs.search.rule = null;
    this.searchForm.patchValue({ruleId: null});
  }

  messageSearch(): void {
    const dialogRef = this.dialog.open(MessageSearchComponent);

    dialogRef.beforeClosed()
      .subscribe((result => {
        if (result.data) {
          this.messageReset();
          this.configs.search.message = result.data;
          this.messageSubject.next(result.data.id);
          this.searchForm.patchValue({messageId: result.data.id});
        }
      }));
  }

  messageReset(): void {
    this.configs.search.message = null;
    this.dataFields = [];
    this.searchForm.patchValue({messageId: null});
  }

  search(): void {
    this.load(true);
  }

  dataFieldSearch(): void {
    const dialogRef = this.dialog.open<DataFieldLogSearchComponent, DataFieldLogSearchData>(
      DataFieldLogSearchComponent,
      {
        data: {message: this.configs.search.message}
      });

    dialogRef.beforeClosed()
      .subscribe((dataField: DataFieldLog) => {
        if (dataField) {

          const find = this.dataFields.find(data => {
            return (data.fieldId === dataField.fieldId)
              && (data.operator === dataField.operator)
              && (data.value === dataField.value);
          });

          if (find) {
            this.dialog.open<DialogConfirmComponent, DialogConfirmData, boolean>(
              DialogConfirmComponent,
              { data: {
                  title: 'Atenção',
                  message: `Filtro '${this.getExpression(dataField)}' já adicionado.`,
                  cancelButton: false,
                  primaryButton: 'OK'
                } }
            );
          } else {
            this.dataFields.push(dataField);
          }

        }
      });
  }

  editDataFieldSearch(dataFieldLog: any): void {
    const idx = this.dataFields.indexOf(dataFieldLog);
    const dialogRef = this.dialog.open<DataFieldLogSearchComponent, DataFieldLogSearchData>(
      DataFieldLogSearchComponent,
      {
        data: {
          message: this.configs.search.message,
          dataField: dataFieldLog
        }
      });

    dialogRef.beforeClosed()
      .subscribe((dataField: DataFieldLog) => {
        if (dataField) {
          this.dataFields[idx] = dataField;
        }
      });
  }

  removeDataFieldFilter(dataField: any): void {
    this.dataFields.splice(this.dataFields.indexOf(dataField), 1);
  }

  reset(): void {
    this.searchForm.reset();
    this.groupConfig.selectGroup = [];
    this.groupConfig.enabled = false;
    this.configs.search.done = false;
    this.configs.search.message = null;
    this.dataFields = [];
    this.load();
  }

  changeDate(input): void {
    if (input.value) {
      this.searchForm.get('dateHourEnd').enable();
    } else {
      const formControl = this.searchForm.get('dateHourEnd');
      formControl.reset();
      formControl.disable();
    }
  }

  refresh(): void {
    this.load();
  }

  getExpression(dataFieldLog: any): string {
    return ConditionUtil.getDataFieldLogExpression(dataFieldLog);
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  view(eventLog: EventLog): void {
    this.dialog.open<EventLogDetailComponent, EventLogDetailData>(EventLogDetailComponent,
      {
        data: {
          eventLog: eventLog
        }
      });
  }

  eventView(eventId: string): void {
    this.dialog.open<EventDetailComponent, EventDetailData>(EventDetailComponent,
      {
        data: {
          view: true,
          event: {id: eventId}
        }
      });
  }

  ruleView(ruleId: string): void {
    this.dialog.open<RuleDetailComponent, RuleDetailData>(RuleDetailComponent,
      {
        data: {
          view: true,
          rule: {id: ruleId}
        }
      });
  }

  messageView(messageId: string): void {
    this.dialog.open<MessageDetailComponent, MessageDetailData>(MessageDetailComponent,
      {
        data: {
          view: true,
          message: {id: messageId}
        }
      });
  }

  getTypeDescription(type: EventType | string): string {
    return EventUtil.getShortDescription(type);
  }

  statistic(): void {

    const groupValues: string[] = [];
    this.displayedColumnsGroup = [];

    this.groupConfig.selected.forEach((group) => {
      const idx = this.groupConfig.all.findIndex(item => item === group);
      if (idx >= 0) {
        groupValues.push(this.groupConfig.allValues[idx]);
      }
    });
    this.displayedColumnsGroup = [...groupValues];
    this.displayedColumnsGroup.push('count');

    const filterBy: EventLog = {
      ...this.searchForm.value,
      dataFields: this.dataFields
    };

    const statisticBy: FilterEventLogStatistic = {
      filters: {...filterBy},
      groupBy: groupValues
    };

    this.configs.inLoadingResults = true;
    this.eventLogService.statistic(statisticBy)
      .pipe(
        tap((data) => {
          this.dataSourceGroup.updateData(data, data.length);
        }),
        catchError((error) => {
          return this.errorService.handleError(error);
        }),
        finalize(() => {
          this.configs.inLoadingResults = false;
        })
      ).subscribe();
  }

  addGroup(event: MatChipInputEvent): void {

    if (!this.matAutocomplete.isOpen) {
      const input = event.input;
      const value = event.value;

      if ((value || '').trim()) {
        if (!this.groupConfig.all.find((item) => item === value)) {
          return;
        }
        this.groupConfig.selected.push(value.trim());
      }

      // Reset the input value
      if (input) {
        input.value = '';
      }

      this.statisticCtrl.setValue(null);
    }
  }

  removeGroup(group: string): void {
    const index = this.groupConfig.selected.indexOf(group);

    if (index >= 0) {
      this.groupConfig.selected.splice(index, 1);
    }
  }

  removeAllGroups(): void {
    this.groupConfig.selected = [];
    this.dataSourceGroup.data = [];
    this.displayedColumnsGroup = [];
  }

  selectGroup(event: MatAutocompleteSelectedEvent): void {
    this.groupConfig.selected.push(event.option.viewValue);
    this.groupByInput.nativeElement.value = '';
    this.statisticCtrl.setValue(null);
  }

  isColumnGroup(column: string) {
    return this.displayedColumnsGroup.find((item) => item === column);
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.groupConfig.all.filter(group => group.toLowerCase().indexOf(filterValue) === 0);
  }

}
