import {Injectable} from '@angular/core';
import {Service} from '../../core/services/service';
import {HttpClient, HttpParams} from '@angular/common/http';
import {EventLog} from '../../core/models/event-log.model';
import {Observable} from 'rxjs';
import {EventLogStatisticView} from '../../core/models/view/event-log-statistic-view.model';
import {FilterEventLogStatistic} from '../../core/models/filters/filter-event-log-statistic';
import {Page} from '../../core/models/page.model';

@Injectable({
  providedIn: 'root'
})
export class EventLogService extends Service<EventLog> {

  private _http: HttpClient;

  constructor(
    http: HttpClient
  ) {
    super('/api/event/logs', http);
    this._http = http;
  }

  statistic(statisticBy: FilterEventLogStatistic): Observable<EventLogStatisticView[]> {
    return this._http.post<EventLogStatisticView[]>(`${this.url}/statistic`, statisticBy);
  }

  find(data?: EventLog, size?: number | 20, page?: number | 0): Observable<Page> {
    return this._http.post<Page>(`${this.url}/find`,
      data,
      {
        params: new HttpParams()
          .set('page', page ? page.toString() : '0')
          .set('size', size ? size.toString() : '20')
      });
  }

}
