import {Injectable} from '@angular/core';
import {Service} from '../../core/services/service';
import {Component, Token} from '../../core/models/component.model';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ComponentService extends Service<Component> {

  private _http: HttpClient;

  constructor(
    http: HttpClient
  ) {
    super('/api/components', http);
    this._http = http;
  }

  resetToken(id: String): Observable<Response> {
    return this._http.get<Response>(`${this.url}/${id}/reset/token`);
  }

  signatureToken(id: String): Observable<Token> {
    return this._http.get<Token>(`${this.url}/${id}/token/signature`);
  }

}
