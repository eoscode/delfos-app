import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ComponentListComponent} from './components/component-list/component-list.component';
import {AuthGuard} from '../login/auth.guard';

const routes: Routes = [
  {
    path: '',
    component: ComponentListComponent,
    canActivate: [ AuthGuard ],
    canActivateChild: [ AuthGuard ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ComponentRoutingModule { }
