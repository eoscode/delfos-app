import {Component, OnInit, ViewChild} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import {HttpDataSource} from '../../../shared/components/http-data-source/http-data.source';

import {ErrorService} from '../../../core/services/error.service';
import {ComponentService} from '../../services/component.service';

import {Component as ComponentModel} from '../../../core/models/component.model';
import {catchError, finalize, map, take, tap} from 'rxjs/operators';
import {ComponentDetailComponent} from '../component-detail/component-detail.component';
import {DialogConfirmComponent, DialogConfirmData} from '../../../shared/components/dialog-confirm/dialog-confirm.component';


@Component({
  selector: 'app-component-list',
  templateUrl: './component-list.component.html',
  styleUrls: ['./component-list.component.scss']
})
export class ComponentListComponent implements OnInit {


  displayedColumns = ['name', 'description', 'enabled', 'actions'];
  dataSource = new HttpDataSource<ComponentModel>();

  configs = {
    resultsLength: 0,
    inLoadingResults: false,
    isRateLimitReached: false,
    pageable: {
      pageIndex: 0,
      pageSize: 20,
      length: 0
    }
  };

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(
    private componentService: ComponentService,
    private errorService: ErrorService,
    public snackBar: MatSnackBar,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    this.configs.inLoadingResults = true;
    this.dataSource.paginator = this.paginator;

    this.load();

    this.paginator.page.subscribe(() => {
      this.load();
    });

    this.dataSource.filterPredicate =
      (data: ComponentModel, filter: string) => {
        return (data.name.toLowerCase().indexOf(filter) !== -1);

      };

  }

  private load(): void {

    this.configs.inLoadingResults = true;
    this.componentService.findAllPages()
      .pipe(
        map(res => {
          this.configs.pageable.length = res.total;

          if (res.pageable) {
            this.configs.pageable.pageIndex = res.pageable.page;
          }

          return res.content || [];
        }),
        tap((data) => {
          this.configs.resultsLength = data.length;
          this.dataSource.updateData(data, this.configs.pageable.length);
        }),
        catchError((error) => {
          return this.errorService.handleError(error);
        }),
        finalize(() => {
          this.configs.inLoadingResults = false;
        })
      ).subscribe();

  }

  refresh(): void {
    this.load();
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  add(): void {
    const dialogRef = this.dialog.open(ComponentDetailComponent, {
      data: {title: 'Novo Componente'}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.successful) {
        this.refresh();
        this.snackBar.open(`Componente cadastrado com sucesso.`,
          'OK', {verticalPosition: 'top', duration: 5000});
        this.refresh();
      }
    });

  }

  edit(component: ComponentModel): void {
   const  model = {...component};
    const dialogRef = this.dialog.open(ComponentDetailComponent, {
      data: {title: 'Alterar Componente', component: model}
    });

    dialogRef.afterClosed()
      .subscribe(result => {
        if (result.successful) {
          this.snackBar.open(`Componente atualizado com sucesso.`,
            'OK', {verticalPosition: 'top', duration: 5000});
          this.refresh();
        }
      });

  }

  delete(component: ComponentModel) {
    const dialogRef = this.dialog.open<DialogConfirmComponent, DialogConfirmData, boolean>(
      DialogConfirmComponent,
      { data: { title: 'Deletar', message: `Deseja excluir o componente '${component.name}'?` } }
    );

    dialogRef.beforeClosed()
      .pipe(take(1))
      .subscribe((confirmed: boolean) => {
        if (confirmed) {
          this.componentService.delete(component.id)
            .pipe(
              take(1),
              tap(() => {
                this.refresh();
                this.snackBar.open(`Componente excluído com sucesso.`,
                  'OK', {verticalPosition: 'top', duration: 5000});
              }),
              catchError((error) => {
                return this.errorService.handleError(error);
              })
            ).subscribe();
        }
      });

  }

  resetToken(component: ComponentModel): void {

    const dialogRef = this.dialog.open<DialogConfirmComponent, DialogConfirmData, boolean>(
      DialogConfirmComponent,
      { data: { title: 'Reset de Token', message: `Deseja gerar novo token para o componente '${component.name}'?` } }
    );

    dialogRef.beforeClosed()
      .pipe(take(1))
      .subscribe((confirmed: boolean) => {
        if (confirmed) {
          this.configs.inLoadingResults = true;
          this.componentService.resetToken(component.id)
            .pipe(
              tap(() => {
                this.snackBar.open(`Token gerado com sucesso.`,
                  'OK', {verticalPosition: 'top', duration: 5000});
              }),
              catchError((error) => {
                return this.errorService.handleError(error);
              }),
              finalize(() => {
                this.configs.inLoadingResults = false;
              })
            ).subscribe();
        }
      });

  }

  signatureToken(component: ComponentModel): void {
    const dialogRef = this.dialog.open<DialogConfirmComponent, DialogConfirmData, boolean>(
      DialogConfirmComponent,
      { data: { title: 'Autenticação', message: `Deseja gerar token de autenticação para o componente '${component.name}'?` } }
    );

    dialogRef.beforeClosed()
      .pipe(take(1))
      .subscribe((confirmed: boolean) => {
        if (confirmed) {
          this.configs.inLoadingResults = true;
          this.componentService.signatureToken(component.id)
            .pipe(
              tap((data) => {
                this.dialog.open<DialogConfirmComponent, DialogConfirmData, boolean>(
                  DialogConfirmComponent,
                  {
                    data: {
                      title: 'Token de Autenticação', message: data.token,
                      cancelButton: false,
                      primaryButton: 'Ok'
                    },
                    maxWidth: 400
                  }
                );
              }),
              catchError((error) => {
                return this.errorService.handleError(error);
              }),
              finalize(() => {
                this.configs.inLoadingResults = false;
              })
            ).subscribe();
        }
      });
  }

}
