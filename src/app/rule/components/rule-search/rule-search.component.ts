import {Component, OnInit, Optional} from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import {Rule} from '../../../core/models/rule.model';
import {FormControl} from '@angular/forms';
import {RuleService} from '../../services/rule.service';
import {ErrorService} from '../../../core/services/error.service';
import {catchError, debounceTime, distinctUntilChanged, filter, finalize, tap} from 'rxjs/operators';

@Component({
  selector: 'app-search-rule',
  templateUrl: './rule-search.component.html',
  styleUrls: ['./rule-search.component.scss']
})
export class RuleSearchComponent implements OnInit {

  displayedColumns = ['name', 'description', 'actions'];
  dataSource = new MatTableDataSource<Rule>();

  filter = new FormControl();

  configs = {
    inLoading: false,
    resultSearch: false
  };

  rule: Rule = null;

  constructor(
    @Optional() public dialogRef: MatDialogRef<RuleSearchComponent>,
    private ruleService: RuleService,
    private errorService: ErrorService,
  ) { }

  ngOnInit() {
    this.dialogRef.disableClose = true;

    this.filter.valueChanges
      .pipe(
        debounceTime(400),
        distinctUntilChanged(),
        filter((query: string) => query && query.length > 2),
        tap((value) => {
          this.configs.inLoading = true;
          this.ruleService.search(value)
            .pipe(
              tap((data) => {
                this.dataSource.data = data;
                this.configs.resultSearch = true;
              }),
              catchError((error) => {
                return this.errorService.handleError(error);
              }),
              finalize(() => {
                this.configs.inLoading = false;
              })
            ).subscribe();
        })
      ).subscribe();

  }

  select(rule: Rule): void {
    this.rule = rule;
  }

  onSelected(rule?: Rule): void {
    this.dialogRef.close({data: rule ? rule : this.rule});
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
