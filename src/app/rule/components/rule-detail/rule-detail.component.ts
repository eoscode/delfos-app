import {Component, Inject, OnInit, Optional} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import {MessageSearchComponent} from '../../../message/components/message-search/message-search.component';
import {MessageService} from '../../../message/services/message.service';
import {RuleService} from '../../services/rule.service';
import {ErrorService} from '../../../core/services/error.service';

import {
  RuleMessageFieldConditionComponent,
  RuleMessageFieldConditionData
} from '../rule-message-field-condition/rule-message-field-condition.component';
import {DialogConfirmComponent, DialogConfirmData} from '../../../shared/components/dialog-confirm/dialog-confirm.component';

import {Rule} from '../../../core/models/rule.model';
import {Condition, ConditionType, OperatorType} from '../../../core/models/condition.model';
import {ConditionUtil} from '../../../core/utils/condition-util';

import {Message} from '../../../core/models/message.model';
import {StateType} from '../../../core/models/state-type';

import {catchError, finalize, take, tap} from 'rxjs/operators';
import {ReplaySubject} from 'rxjs';


@Component({
  selector: 'app-rule-detail',
  templateUrl: './rule-detail.component.html',
  styleUrls: ['./rule-detail.component.scss']
})
export class RuleDetailComponent implements OnInit {

  message: Message;
  rule: Rule = {
    conditions: []
  };
  formGroup: FormGroup;

  displayedColumns = ['andOr', 'expression', 'actions'];
  dataSource = new MatTableDataSource<Condition>();

  configs = {
    inLoading: false,
    inSaving: false,
    message: ''
  };

  private messageSubject = new ReplaySubject<string>(1);

  constructor(
    @Optional() public dialogRef: MatDialogRef<RuleDetailComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: RuleDetailData,
    private dialog: MatDialog,
    private formBuilder: FormBuilder,
    private ruleService: RuleService,
    private messageService: MessageService,
    private errorService: ErrorService,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    if (this.dialogRef) {
      this.dialogRef.disableClose = true;
    }
    this.createForm();

    this.messageSubject.subscribe((messageName => {
      this.configs.inLoading = true;
      this.messageService.findDetailByName(messageName)
        .pipe(
          tap((result: Message) => {
            this.message = result;
            this.configs.message = this.message.name;
          }),
          catchError((error) => {
            return this.errorService.handleError(error);
          }),
          finalize(() => {
            this.configs.inLoading = false;
          })
        ).subscribe();
    }));

    if (this.data.rule) {
      this.rule = this.data.rule;

      this.configs.inLoading = true;
      this.ruleService.findDetailById(this.rule.id)
        .pipe(
          tap((data) => {
            this.rule = data;
            this.dataSource.data = data.conditions;
            this.loadMessageCondition(data.conditions);

            this.formGroup.patchValue({
              ...this.rule,
              state: this.rule.state === StateType.ENABLED
            });
          }),
          catchError((error) => {
            return this.errorService.handleError(error);
          }),
          finalize(() => {
            this.configs.inLoading = false;
          })
        ).subscribe();

      this.formGroup.patchValue({
        ...this.rule,
        state: this.rule.state === StateType.ENABLED
      });
      this.dataSource.data = this.rule.conditions;
    }

    if (this.data.view) {
      this.formGroup.get('state').disable();
    }

  }

  private loadMessageCondition(conditions: Condition[]): void {
    if (conditions) {
      const findCondition: Condition = this.rule.conditions.find((condition) => condition.type === ConditionType.MESSAGE);
      if (findCondition) {
        this.messageSubject.next(findCondition.value);
      }
    }
  }

  private createForm(): void {
    this.formGroup = this.formBuilder.group({
      name: [null, Validators.compose([Validators.required, Validators.minLength(3)])],
      description: [null],
      state: [false]
    });
  }

  private addMessageCondition(message: Message): void {
    const condition = new Condition();

    condition.type = 1;
    condition.operator = OperatorType.EQ;
    condition.expressionType = 1;
    condition.value = message.name;

    this.addCondition(condition);
  }

  private addCondition(condition: Condition, parent?: Condition): void {
    if (parent) {
      condition.order = parent.conditions.length;
      condition.parentId = parent.id;
      parent.conditions.push(condition);
    } else {
      condition.order = this.rule.conditions.length;
      this.rule.conditions.push(condition);
    }

    this.dataSource.data = this.rule.conditions;
  }

  get inLoading(): boolean {
    return this.configs.inLoading || this.configs.inSaving;
  }

  get existsMessage(): boolean {
    return this.message != null;
  }

  searchMessage(): void {
    const dialogRef = this.dialog.open(MessageSearchComponent);

    dialogRef.beforeClosed()
      .subscribe((result => {
        if (result.data) {
          this.addMessageCondition(result.data);
          this.messageSubject.next(result.data.name);
        }
      }));
  }

  addMessageFieldCondition(parent?: Condition | null): void {
    const dialogRef = this.dialog.open<RuleMessageFieldConditionComponent, RuleMessageFieldConditionData>(
      RuleMessageFieldConditionComponent,
      {
        data: {
          message: this.message,
          parent: parent
        }
      });

    dialogRef.beforeClosed()
      .subscribe((condition: Condition) => {
        if (condition) {
          this.addCondition(condition, parent);
        }
      });

  }

  addGroupCondition(): void {
    const condition = new Condition();
    condition.type = ConditionType.GROUP;
    condition.andOr = 'and';
    condition.conditions = [];

    this.addCondition(condition);
  }

  getExpression(condition: Condition): string {
    return ConditionUtil.getExpression(condition);
  }

  isGroupOrMessage(condition: Condition): boolean {
    return condition.type === ConditionType.GROUP || condition.type === ConditionType.MESSAGE;
  }

  edit(condition: Condition, parent?: Condition): void {

    if (this.data.view) {
      return;
    }

    if (condition.type === ConditionType.MESSAGE) {
      return;
    }
    const idx = parent ? parent.conditions.indexOf(condition) : this.rule.conditions.indexOf(condition);

    const dialogRef = this.dialog.open<RuleMessageFieldConditionComponent, RuleMessageFieldConditionData>(
      RuleMessageFieldConditionComponent,
      {
        data: {
          message: this.message,
          condition: condition
        }
      });

    dialogRef.beforeClosed()
      .subscribe((result: Condition) => {
        if (result) {
          if (parent) {
            parent.conditions[idx] = result;
          } else {
            this.rule.conditions[idx] = result;
          }
          this.dataSource.data = this.rule.conditions;
        }
      });
  }

  delete(condition: Condition, parent?: Condition): void {
    const dialogRef = this.dialog.open<DialogConfirmComponent, DialogConfirmData, boolean>(
      DialogConfirmComponent,
      { data: { title: 'Deletar', message: `Deseja excluir a condição '${this.getExpression(condition)}'?` } }
    );

    dialogRef.beforeClosed()
      .pipe(take(1))
      .subscribe((confirmed: boolean) => {
        if (confirmed) {

          let rowRemoved = false;
          if (parent != null) {
            parent.conditions.splice(parent.conditions.indexOf(condition), 1);

            if (parent.conditions.length === 0) {
              this.rule.conditions.splice(this.rule.conditions.indexOf(parent), 1);
              rowRemoved = true;
            } else {
              let order = 0;
              parent.conditions.forEach((item) => {
                item.order = order;
                 order = order + 1;
              });
            }
          } else {
            this.rule.conditions.splice(this.rule.conditions.indexOf(condition), 1);
            rowRemoved = true;
          }

          if (rowRemoved && this.rule.conditions.length > 0) {
            let order = 0;
            this.rule.conditions.forEach((item) => {
              item.order = order;
              order = order + 1;
            });
          }

          this.dataSource.data = this.rule.conditions;

          this.snackBar.open(`Condição excluída com sucesso.`,
            'OK', {verticalPosition: 'top', duration: 5000});
        }
      });
  }

  changeGroupCondition(condition: Condition, value?: string): void {
    if (value) {
      condition.andOr = value;
    } else {
      condition.andOr = condition.andOr === 'or' ? 'and' : 'or';
    }
  }

  onSubmit(): void {
    this.configs.inSaving = true;

    const model: Rule = {
      ...this.rule,
      ...this.formGroup.value
    };
    model.state = (model.state ? StateType.ENABLED : StateType.DISABLED);

    model.conditions.forEach((condition) => {
      if (condition.type === ConditionType.FIELD) {
        condition.fieldId = condition.field.id;
        condition.field = {id: condition.field.id, name: condition.field.name};
      }
    });


    this.ruleService.save(model, model.id)
      .pipe(
        tap((data) => {
          this.configs.inSaving = false;
          this.dialogRef.close({successful: true, data: data});
        }),
        catchError((error) => {
          this.configs.inSaving = false;
          return this.errorService.handleError(error);
        }),
        finalize( () => {
          this.configs.inSaving = false;
          this.configs.inLoading = false;
        })
      ).subscribe();

  }

  onNoClick(): void {
    this.dialogRef.close({});
  }

}


export class RuleDetailData {
  title?: string;
  view?: boolean | false;
  rule: Rule;
}
