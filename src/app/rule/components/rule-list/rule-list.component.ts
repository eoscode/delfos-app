import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {MatPaginator} from '@angular/material/paginator';
import {MatSnackBar} from '@angular/material/snack-bar';
import {DialogConfirmComponent, DialogConfirmData} from '../../../shared/components/dialog-confirm/dialog-confirm.component';
import {RuleDetailComponent} from '../rule-detail/rule-detail.component';
import {HttpDataSource} from '../../../shared/components/http-data-source/http-data.source';
import {RuleService} from '../../services/rule.service';
import {ErrorService} from '../../../core/services/error.service';

import {Rule} from '../../../core/models/rule.model';
import {catchError, finalize, map, take, tap} from 'rxjs/operators';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MatExpansionPanel} from '@angular/material';
import {RuleSearchComponent} from '../rule-search/rule-search.component';


@Component({
  selector: 'app-rule-list',
  templateUrl: './rule-list.component.html',
  styleUrls: ['./rule-list.component.scss']
})
export class RuleListComponent implements OnInit {

  displayedColumns = ['name', 'description', 'state', 'actions'];
  dataSource = new HttpDataSource<Rule>();

  selectable = true;
  removable = true;

  configs = {
    resultsLength: 0,
    inLoadingResults: false,
    isRateLimitReached: false,
    pageable: {
      pageIndex: 0,
      pageSize: 20,
      length: 0
    },
    search: {
      done: false,
      rule: null,
      selectable: true,
      removable: true
    }
  };

  searchForm: FormGroup;

  @ViewChild('filterPanel', { static: true }) filterPanel: MatExpansionPanel;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(
    private ruleService: RuleService,
    private errorService: ErrorService,
    public snackBar: MatSnackBar,
    public dialog: MatDialog,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.configs.inLoadingResults = true;
    this.dataSource.paginator = this.paginator;

    this.createSearchForm();

    this.load();

    this.paginator.page.subscribe(() => {
      this.load();
    });

    this.dataSource.filterPredicate =
      (data: Rule, filter: string) => {
        return (data.name.toLowerCase().indexOf(filter) !== -1);
      };
  }

  private load(search?: boolean): void {
    if (search) {
      this.paginator.pageIndex = 0;
    }

    this.configs.inLoadingResults = true;
    this.ruleService.find({...this.searchForm.value})
      .pipe(
        map(res => {
          this.configs.pageable.length = res.total;

          if (res.pageable) {
            this.configs.pageable.pageIndex = res.pageable.page;
          }

          return res.content || [];
        }),
        tap((data) => {
          this.configs.resultsLength = data.length;
          this.dataSource.updateData(data, this.configs.pageable.length);

          if (search && data.length > 0) {
            this.filterPanel.close();
            this.configs.search.done = true;
          }

        }),
        catchError((error) => {
          return this.errorService.handleError(error);
        }),
        finalize(() => {
          this.configs.inLoadingResults = false;
        })
      ).subscribe();

  }

  private createSearchForm(): void {
    this.searchForm = this.formBuilder.group({
      id: [null],
    });
  }

  refresh(): void {
    this.load();
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  add(): void {
    const dialogRef = this.dialog.open(RuleDetailComponent, {
      data: {title: 'Nova Regra'}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.successful) {
        this.refresh();
        this.snackBar.open(`Regra cadastrada com sucesso.`,
          'OK', {verticalPosition: 'top', duration: 5000});
        this.refresh();
      }
    });

  }

  edit(rule: Rule): void {
    const  model = {...rule};
    const dialogRef = this.dialog.open(RuleDetailComponent, {
      data: {title: 'Alterar Regra', rule: model}
    });

    dialogRef.afterClosed()
      .subscribe(result => {
        if (result.successful) {
          this.snackBar.open(`Regra atualizada com sucesso.`,
            'OK', {verticalPosition: 'top', duration: 5000});
          this.refresh();
        }
      });

  }

  delete(rule: Rule) {
    const dialogRef = this.dialog.open<DialogConfirmComponent, DialogConfirmData, boolean>(
      DialogConfirmComponent,
      { data: { title: 'Deletar', message: `Deseja excluir a regra '${rule.name}'?` } }
    );

    dialogRef.beforeClosed()
      .pipe(take(1))
      .subscribe((confirmed: boolean) => {
        if (confirmed) {
          this.ruleService.delete(rule.id)
            .pipe(
              take(1),
              tap(() => {
                this.refresh();
                this.snackBar.open(`Regra excluída com sucesso.`,
                  'OK', {verticalPosition: 'top', duration: 5000});
              }),
              catchError((error) => {
                return this.errorService.handleError(error);
              })
            ).subscribe();
        }
      });
  }

  ruleSearch(): void {
    const dialogRef = this.dialog.open(RuleSearchComponent);

    dialogRef.beforeClosed()
      .subscribe((result => {
        if (result.data) {
          this.configs.search.rule = result.data;
          this.searchForm.patchValue({id: result.data.id});
        }
      }));
  }

  ruleReset(): void {
    this.configs.search.rule = null;
    this.searchForm.patchValue({id: null});
  }

  search(): void {
    this.load(true);
  }

  reset(): void {
    this.searchForm.reset();
    this.configs.search.rule = null;
    this.configs.search.done = false;
    this.load();
  }

}
