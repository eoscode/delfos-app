import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import {Condition, ConditionType, OperatorType} from '../../../core/models/condition.model';
import {Message} from '../../../core/models/message.model';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ConditionUtil} from '../../../core/utils/condition-util';
import {FieldType} from '../../../core/models/field-type.model';

@Component({
  selector: 'app-rule-message-field-condition',
  templateUrl: './rule-message-field-condition.component.html',
  styleUrls: ['./rule-message-field-condition.component.scss']
})
export class RuleMessageFieldConditionComponent implements OnInit {

  @ViewChild('inputValue', { static: false }) inputValue;

  message: Message;
  condition: Condition = {
    expressionType: 0,
    type: ConditionType.FIELD,
    field: null,
    value: null
  };

  formGroup: FormGroup;

  operators: any[];

  configs = {
    inLoading: false
  };

  constructor(
    public dialogRef: MatDialogRef<RuleMessageFieldConditionComponent>,
    @Inject(MAT_DIALOG_DATA) public data: RuleMessageFieldConditionData,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {

    this.createForm();

    if (this.dialogRef) {
      this.dialogRef.disableClose = true;
    }

    if (this.data) {
      if (this.data.message) {
        this.message = this.data.message;
      }

      if ((this.data.parent
        && this.data.parent.conditions.length === 0)
        || (this.data.condition && this.data.condition.order === 0)) {
        this.formGroup.get('andOr').disable();
      }

      if (this.data.condition) {
        this.condition = this.data.condition;
        this.formGroup.patchValue({...this.condition});
        this.changeField(this.condition.fieldId ? this.condition.fieldId : this.condition.field.id);
        this.changeOperator(this.condition.operator);
      }
    }

  }


  private createForm(): void {
    this.formGroup = this.formBuilder.group({
      operator: [null, Validators.required],
      value: [{value: null, disabled: true}, Validators.required],
      andOr: ['', Validators.required],
      fieldId: [null, Validators.required],
    });
  }

  changeField(fieldId): void {
    if (this.message.fields) {
      const fieldCondition = this.message.fields.find((field) => field.id === fieldId);
      if (fieldCondition) {
        this.condition.field = fieldCondition;
        this.operators = ConditionUtil.getOperators(fieldCondition);

        if (this.formGroup.get('fieldId').value !== '') {
          this.formGroup.get('value').enable();
        } else {
          this.formGroup.get('value').disable();
        }

      } else {
        this.formGroup.get('value').disable();
      }
    }
  }

  changeOperator(value: string): void {
    if (value === OperatorType.IS_NOT_NUL
      || value === OperatorType.IS_NULL) {
      this.formGroup.get('value').disable();
      this.formGroup.get('value').reset();
    } else {
      this.formGroup.get('value').enable();
    }
  }

  get inputFieldType(): string {
    if (this.condition.field) {
      if (this.condition.field.type === FieldType.NUMBER
        || this.condition.field.type === 'NUMBER') {
        return 'number';
      }
    }
    return 'text';
  }

  getOperatorDescription(operator: string): string {
    return ConditionUtil.getOperator(operator);
  }

  onConfirm(): void {
    const model: Condition = {
      ...this.condition,
      ...this.formGroup.value
    };
    this.dialogRef.close(model);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}

export class RuleMessageFieldConditionData {
  title?: string;
  message: Message;
  condition?: Condition;
  parent?: Condition;
}
