import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RuleMessageFieldConditionComponent } from './rule-message-field-condition.component';

describe('RuleMessageFieldConditionComponent', () => {
  let component: RuleMessageFieldConditionComponent;
  let fixture: ComponentFixture<RuleMessageFieldConditionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RuleMessageFieldConditionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RuleMessageFieldConditionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
