import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {RuleListComponent} from './components/rule-list/rule-list.component';
import {AuthGuard} from '../login/auth.guard';

const routes: Routes = [
  {
    path: '',
    component: RuleListComponent,
    canActivate: [ AuthGuard ],
    canActivateChild: [ AuthGuard ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RuleRoutingModule { }
