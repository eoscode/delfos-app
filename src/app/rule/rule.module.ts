import {NgModule} from '@angular/core';

import {RuleRoutingModule} from './rule-routing.module';
import {SharedModule} from '../shared/shared.module';
import {RuleDetailComponent} from './components/rule-detail/rule-detail.component';
import {DialogConfirmComponent} from '../shared/components/dialog-confirm/dialog-confirm.component';
import {RuleListComponent} from './components/rule-list/rule-list.component';
import {MessageSearchComponent} from '../message/components/message-search/message-search.component';
import {MessageModule} from '../message/message.module';
import { RuleMessageFieldConditionComponent } from './components/rule-message-field-condition/rule-message-field-condition.component';
import { RuleSearchComponent } from './components/rule-search/rule-search.component';


@NgModule({
  declarations: [RuleListComponent, RuleDetailComponent, RuleMessageFieldConditionComponent, RuleSearchComponent],
  imports: [
    SharedModule,
    RuleRoutingModule,
    MessageModule
  ],
  exports: [
    RuleSearchComponent, RuleDetailComponent
  ],
  entryComponents: [
    DialogConfirmComponent,
    RuleDetailComponent,
    RuleMessageFieldConditionComponent,
    MessageSearchComponent,
    RuleSearchComponent
  ]
})
export class RuleModule { }
