import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Service} from '../../core/services/service';
import {Rule} from '../../core/models/rule.model';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RuleService extends Service<Rule> {

  private _http: HttpClient;

  constructor(
    http: HttpClient
  ) {
    super('/api/rules', http);
    this._http = http;
  }

  search(name: string): Observable<Rule[]> {
    return this._http.get<Rule[]>(`${this.url}/search`,
      {
        params: new HttpParams()
          .set('name', name)
      });
  }

}
