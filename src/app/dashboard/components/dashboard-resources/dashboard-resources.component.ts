import { Component, EventEmitter, OnInit, Input, Output } from '@angular/core';

@Component({
  selector: 'app-dashboard-resources',
  templateUrl: './dashboard-resources.component.html',
  styleUrls: ['./dashboard-resources.component.css']
})
export class DashboardResourcesComponent implements OnInit {

  @Input() isMenu = false;
  @Output() close = new EventEmitter<void>();

  resources: ItemMenu[] = [
    {
      url: '/dashboard',
      icon: 'dashboard',
      title: 'Home'
    },
    {
      divider: true
    },
    {
      url: '/dashboard/components',
      icon: 'devices_other',
      title: 'Componentes'
    },
    {
      url: '/dashboard/messages',
      icon: 'message',
      title: 'Mensagens'
    },
    {
      url: '/dashboard/rules',
      icon: 'query_builder',
      title: 'Regras'
    },
    {
      url: '/dashboard/events',
      icon: 'event_note',
      title: 'Eventos'
    },
    {
      divider: true
    },
    {
      url: '/dashboard/message-logs',
      icon: 'find_in_page',
      title: 'Log de Mensagens'
    },
    {
      url: '/dashboard/event-logs',
      icon: 'warning',
      title: 'Log de Eventos'
    },
    {
      divider: true
    },
    {
      url: '/dashboard/users',
      icon: 'people',
      title: 'Usuários'
    },
    {
      url: '/dashboard/configurations',
      icon: 'settings',
      title: 'Configurações'
    }
  ];

  constructor() { }

  ngOnInit(): void {
    /*
    if (this.isMenu) {
      this.resources.unshift(
        {
          url: '/dashboard',
          icon: 'home',
          title: 'Home'
        }
      );
    }
    */
  }

  onClose(): void {
    this.close.emit();
  }

}

export interface ItemMenu {
  url?: string;
  icon?: string;
  title?: string;
  divider?: boolean | false;
}
