import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {DashboardHomeComponent} from './components/dashboard-home/dashboard-home.component';
import {AuthGuard} from '../login/auth.guard';

const routes: Routes = [
  {
    path: '',
    component: DashboardHomeComponent,
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    children: [
      { path: 'components',
        loadChildren: () => import('./../component/component.module').then(m => m.ComponentModule), canActivate: [ AuthGuard ] },
      { path: 'messages',
        loadChildren: () => import('./../message/message.module').then(m => m.MessageModule), canActivate: [ AuthGuard ] },
      { path: 'rules',
        loadChildren: () => import('./../rule/rule.module').then(m => m.RuleModule), canActivate: [ AuthGuard ] },
      { path: 'events',
        loadChildren: () => import('./../event/event.module').then(m => m.EventModule), canActivate: [ AuthGuard ] },
      { path: 'message-logs',
        loadChildren: () => import('./../message-log/message-log.module').then(m => m.MessageLogModule), canActivate: [ AuthGuard ] },
      { path: 'event-logs',
        loadChildren: () => import('./../event-log/event-log.module').then(m => m.EventLogModule), canActivate: [ AuthGuard ] },
      { path: 'users',
        loadChildren: () => import('./../user/user.module').then(m => m.UserModule), canActivate: [ AuthGuard ] },
      { path: '',
        loadChildren: () => import('./../home/home.module').then(m => m.HomeModule), canActivate: [ AuthGuard ] }

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
