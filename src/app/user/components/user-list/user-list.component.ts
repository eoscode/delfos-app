import {Component, OnInit, ViewChild} from '@angular/core';
import {HttpDataSource} from '../../../shared/components/http-data-source/http-data.source';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import {ErrorService} from '../../../core/services/error.service';
import {catchError, finalize, map, take, tap} from 'rxjs/operators';
import {UserService} from '../../services/user.service';
import {User} from '../../../core/models/user.model';
import {UserDetailComponent, UserDetailData} from '../user-detail/user-detail.component';
import {DialogConfirmComponent, DialogConfirmData} from '../../../shared/components/dialog-confirm/dialog-confirm.component';
import {AuthService} from '../../../core/services/auth.service';
import {UserChangePasswordComponent, UserChangePasswordData} from '../user-change-password/user-change-password.component';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {

  displayedColumns = ['name', 'username', 'actions'];
  dataSource = new HttpDataSource<User>();

  configs = {
    resultsLength: 0,
    inLoadingResults: false,
    isRateLimitReached: false,
    pageable: {
      pageIndex: 0,
      pageSize: 20,
      length: 0
    }
  };

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(
    private userService: UserService,
    private errorService: ErrorService,
    private authService: AuthService,
    public snackBar: MatSnackBar,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    this.configs.inLoadingResults = true;
    this.dataSource.paginator = this.paginator;

    this.load();

    this.paginator.page.subscribe(() => {
      this.load();
    });

    this.dataSource.filterPredicate =
      (data: User, filter: string) => {
        return data.name.toLowerCase().indexOf(filter) !== -1
          || data.username.toLowerCase().indexOf(filter) !== -1;

      };

  }

  private load(): void {

    this.configs.inLoadingResults = true;
    this.userService.findAllPages()
      .pipe(
        map(res => {
          this.configs.pageable.length = res.total;

          if (res.pageable) {
            this.configs.pageable.pageIndex = res.pageable.page;
          }

          return res.content || [];
        }),
        tap((data) => {
          this.configs.resultsLength = data.length;
          this.dataSource.updateData(data, this.configs.pageable.length);
        }),
        catchError((error) => {
          return this.errorService.handleError(error);
        }),
        finalize(() => {
          this.configs.inLoadingResults = false;
        })
      ).subscribe();

  }

  get authorizedUser(): User {
    return this.authService.user;
  }

  refresh(): void {
    this.load();
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  add(): void {
    const dialogRef = this.dialog.open<UserDetailComponent, UserDetailData>(UserDetailComponent, {
      data: {title: 'Novo Usuário'}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.successful) {
        this.refresh();
        this.snackBar.open(`Usuário cadastrado com sucesso.`,
          'OK', {verticalPosition: 'top', duration: 5000});
        this.refresh();
      }
    });

  }

  edit(user: User): void {
    const  model = {...user};
    const dialogRef = this.dialog.open<UserDetailComponent, UserDetailData>(UserDetailComponent, {
      data: {title: 'Alterar Usuário', user: model}
    });

    dialogRef.afterClosed()
      .subscribe(result => {
        if (result.successful) {
          this.snackBar.open(`Usuário atualizado com sucesso.`,
            'OK', {verticalPosition: 'top', duration: 5000});
          this.refresh();
        }
      });
  }

  delete(user: User): void {
   const dialogRef = this.dialog.open<DialogConfirmComponent, DialogConfirmData, boolean>(
      DialogConfirmComponent,
      { data: { title: 'Deletar', message: `Deseja excluir o usuário '${user.name}'?` } }
    );

    dialogRef.beforeClosed()
      .pipe(take(1))
      .subscribe((confirmed: boolean) => {
        if (confirmed) {
          this.userService.delete(user.id)
            .pipe(
              take(1),
              tap(() => {
                this.refresh();
                this.snackBar.open(`Usuário excluído com sucesso.`,
                  'OK', {verticalPosition: 'top', duration: 5000});
              }),
              catchError((error) => {
                return this.errorService.handleError(error);
              })
            ).subscribe();
        }
      });

  }

  changePassword(user: User): void {
    this.dialog.open<UserChangePasswordComponent, UserChangePasswordData>(UserChangePasswordComponent, {
      data: {
        title: user.name,
        user: user
      }
    });
  }

}
