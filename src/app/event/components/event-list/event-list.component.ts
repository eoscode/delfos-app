import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatSnackBar} from '@angular/material/snack-bar';
import {HttpDataSource} from '../../../shared/components/http-data-source/http-data.source';
import {EventService} from '../../services/event.service';
import {ErrorService} from '../../../core/services/error.service';
import {catchError, finalize, map, take, tap} from 'rxjs/operators';
import {Event} from '../../../core/models/event.model';
import {EventDetailComponent} from '../event-detail/event-detail.component';
import {DialogConfirmComponent, DialogConfirmData} from '../../../shared/components/dialog-confirm/dialog-confirm.component';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MatExpansionPanel} from '@angular/material/expansion';
import {RuleSearchComponent} from '../../../rule/components/rule-search/rule-search.component';
import {EventSearchComponent} from '../event-search/event-search.component';
import {FilterDefinition} from '../../../core/models/filters/filter-definition';


@Component({
  selector: 'app-event-list',
  templateUrl: './event-list.component.html',
  styleUrls: ['./event-list.component.scss']
})
export class EventListComponent implements OnInit {

  displayedColumns = ['name', 'description', 'state', 'actions'];
  dataSource = new HttpDataSource<Event>();

  configs = {
    resultsLength: 0,
    inLoadingResults: false,
    isRateLimitReached: false,
    pageable: {
      pageIndex: 0,
      pageSize: 20,
      length: 0
    },
    search: {
      done: false,
      event: null,
      rule: null,
      selectable: true,
      removable: true
    }
  };

  searchForm: FormGroup;

  @ViewChild('filterPanel', { static: true }) filterPanel: MatExpansionPanel;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(
    private eventService: EventService,
    private errorService: ErrorService,
    private formBuilder: FormBuilder,
    public snackBar: MatSnackBar,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    this.configs.inLoadingResults = true;
    this.dataSource.paginator = this.paginator;

    this.createSearchForm();

    this.paginator.page.subscribe((event: PageEvent) => {
      this.load();
    });

    this.load();

    this.dataSource.filterPredicate =
      (data: Event, filter: string) => {
        return (data.name.toLowerCase().indexOf(filter) !== -1);
      };
  }

  private load(search?: boolean): void {

    const filters: FilterDefinition[] = [];
    if (search) {
      this.paginator.pageIndex = 0;

      if (this.searchForm.value['id'] != null) {
        filters.push({name: 'id', operator: '=', value: this.searchForm.value['id']});
      }

      if (this.searchForm.value['ruleId'] != null) {
        filters.push({name: 'rules.id', operator: '=', value: this.searchForm.value['ruleId']});
      }

    }

    this.configs.inLoadingResults = true;
    this.eventService.filter(filters)
      .pipe(
        map(res => {
          this.configs.pageable.length = res.total;

          if (res.pageable) {
            this.configs.pageable.pageIndex = res.pageable.page;
          }

          return res.content || [];
        }),
        tap((data) => {
          this.configs.resultsLength = data.length;
          this.dataSource.updateData(data, this.configs.pageable.length);
        }),
        catchError((error) => {
          return this.errorService.handleError(error);
        }),
        finalize(() => {
          this.configs.inLoadingResults = false;
        })
      ).subscribe();

  }

  private createSearchForm(): void {
    this.searchForm = this.formBuilder.group({
      id: [null],
      ruleId: [null]
    });
  }

  refresh(): void {
    this.load();
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  add(): void {
    const dialogRef = this.dialog.open(EventDetailComponent, {
      data: {title: 'Novo Evento'}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.successful) {
        this.refresh();
        this.snackBar.open(`Evento cadastrado com sucesso.`,
          'OK', {verticalPosition: 'top', duration: 5000});
        this.refresh();
      }
    });

  }

  edit(event: Event): void {
    const  model = {...event};
    const dialogRef = this.dialog.open(EventDetailComponent, {
      data: {title: 'Alterar Evento', event: model}
    });

    dialogRef.afterClosed()
      .subscribe(result => {
        if (result.successful) {
          this.snackBar.open(`Evento atualizado com sucesso.`,
            'OK', {verticalPosition: 'top', duration: 5000});
          this.refresh();
        }
      });

  }

  delete(event: Event) {
    const dialogRef = this.dialog.open<DialogConfirmComponent, DialogConfirmData, boolean>(
      DialogConfirmComponent,
      { data: { title: 'Deletar', message: `Deseja excluir o evento '${event.name}'?` } }
    );

    dialogRef.beforeClosed()
      .pipe(take(1))
      .subscribe((confirmed: boolean) => {
        if (confirmed) {
          this.eventService.delete(event.id)
            .pipe(
              take(1),
              tap(() => {
                this.refresh();
                this.snackBar.open(`Evento excluido com sucesso.`,
                  'OK', {verticalPosition: 'top', duration: 5000});
              }),
              catchError((error) => {
                return this.errorService.handleError(error);
              })
            ).subscribe();
        }
      });
  }

  ruleSearch(): void {
    const dialogRef = this.dialog.open(RuleSearchComponent);

    dialogRef.beforeClosed()
      .subscribe((result => {
        if (result.data) {
          this.configs.search.rule = result.data;
          this.searchForm.patchValue({ruleId: result.data.id});
        }
      }));
  }

  ruleReset(): void {
    this.configs.search.rule = null;
    this.searchForm.patchValue({ruleId: null});
  }

  eventSearch(): void {
    const dialogRef = this.dialog.open(EventSearchComponent);

    dialogRef.beforeClosed()
      .subscribe((result => {
        if (result.data) {
          this.configs.search.event = result.data;
          this.searchForm.patchValue({id: result.data.id});
        }
      }));
  }

  eventReset(): void {
    this.configs.search.event = null;
    this.searchForm.patchValue({id: null});
  }

  search(): void {
    this.load(true);
  }

  reset(): void {
    this.searchForm.reset();
    this.configs.search.event = null;
    this.configs.search.rule = null;
    this.configs.search.done = false;
    this.load();
  }

}
