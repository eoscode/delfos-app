import {Component, OnInit, Optional} from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import {Event} from '../../../core/models/event.model';
import {FormControl} from '@angular/forms';
import {ErrorService} from '../../../core/services/error.service';
import {catchError, debounceTime, distinctUntilChanged, filter, finalize, tap} from 'rxjs/operators';
import {EventService} from '../../services/event.service';

@Component({
  selector: 'app-event-search',
  templateUrl: './event-search.component.html',
  styleUrls: ['./event-search.component.scss']
})
export class EventSearchComponent implements OnInit {

  displayedColumns = ['name', 'description'];
  dataSource = new MatTableDataSource<Event>();

  filter = new FormControl();

  configs = {
    inLoading: false,
    resultSearch: false
  };

  event: Event = null;

  constructor(
    @Optional() public dialogRef: MatDialogRef<EventSearchComponent>,
    private eventService: EventService,
    private errorService: ErrorService,
  ) { }

  ngOnInit() {
    this.dialogRef.disableClose = true;

    this.filter.valueChanges
      .pipe(
        debounceTime(400),
        distinctUntilChanged(),
        filter((query: string) => query && query.length > 2),
        tap((value) => {
          this.configs.inLoading = true;
          this.eventService.findByNameAndDescription(value)
            .pipe(
              tap((data) => {
                this.dataSource.data = data;
                this.configs.resultSearch = true;
              }),
              catchError((error) => {
                return this.errorService.handleError(error);
              }),
              finalize(() => {
                this.configs.inLoading = false;
              })
            ).subscribe();
        })
      ).subscribe();

  }

  select(event: Event): void {
    this.event = event;
  }

  onSelected(event?: Event): void {
    this.dialogRef.close({data: event ? event : this.event});
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
