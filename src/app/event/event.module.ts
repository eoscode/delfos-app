import {NgModule} from '@angular/core';
import {SharedModule} from '../shared/shared.module';
import {EventRoutingModule} from './event-routing.module';
import {EventListComponent} from './components/event-list/event-list.component';
import {EventDetailComponent} from './components/event-detail/event-detail.component';
import {RuleModule} from '../rule/rule.module';
import {RuleSearchComponent} from '../rule/components/rule-search/rule-search.component';
import {DialogConfirmComponent} from '../shared/components/dialog-confirm/dialog-confirm.component';
import {EventSearchComponent} from './components/event-search/event-search.component';


@NgModule({
  declarations: [EventListComponent, EventDetailComponent, EventSearchComponent],
  imports: [
    SharedModule,
    EventRoutingModule,
    RuleModule
  ],
  exports: [
    EventSearchComponent,
    EventDetailComponent
  ],
  entryComponents: [
    EventDetailComponent, RuleSearchComponent, DialogConfirmComponent, RuleSearchComponent, EventSearchComponent
  ]
})
export class EventModule { }
