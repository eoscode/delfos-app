import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Service} from '../../core/services/service';
import {Message} from '../../core/models/message.model';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MessageService extends Service<Message> {

  private _http: HttpClient;

  constructor(
    http: HttpClient
  ) {
    super('/api/messages', http);
    this._http = http;
  }

  findDetailByName(name: string): Observable<Message> {
    return this._http.get<Message>(`${this.url}/detail/name/${name}`);
  }

  search(name: string): Observable<Message[]> {
    return this._http.get<Message[]>(`${this.url}/search`,
      {
        params: new HttpParams()
          .set('name', name)
      });
  }

}
