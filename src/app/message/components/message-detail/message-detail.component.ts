import {Component, Inject, OnInit, Optional} from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MessageService} from '../../services/message.service';
import {ErrorService} from '../../../core/services/error.service';

import {MessageFieldDetailComponent} from '../message-field-detail/message-field-detail.component';
import {DialogConfirmComponent, DialogConfirmData} from '../../../shared/components/dialog-confirm/dialog-confirm.component';

import {Message} from '../../../core/models/message.model';
import {Field} from '../../../core/models/field.model';
import {catchError, finalize, take, tap} from 'rxjs/operators';


@Component({
  selector: 'app-message-detail',
  templateUrl: './message-detail.component.html',
  styleUrls: ['./message-detail.component.scss']
})
export class MessageDetailComponent implements OnInit {

  message: Message = {
    fields: []
  };
  formGroup: FormGroup;

  displayedColumns = ['name', 'type', 'decimalPoint', 'actions'];
  dataSource = new MatTableDataSource<Field>();

  filter = new FormControl();
  filteredValues = { name: ''};

  configs = {
    inLoading: false,
    inSaving: false
  };

  constructor(
    @Optional() public dialogRef: MatDialogRef<MessageDetailComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: MessageDetailData,
    private formBuilder: FormBuilder,
    private messageService: MessageService,
    private errorService: ErrorService,
    private snackBar: MatSnackBar,
    private dialog: MatDialog
  ) { }

  ngOnInit() {
    if (this.dialogRef) {
      this.dialogRef.disableClose = true;
    }
    this.createForm();

    this.filter.valueChanges
      .subscribe(value => {
        this.filteredValues['name'] = value;
        this.dataSource.filter = JSON.stringify(this.filteredValues);
      });

    this.dataSource.filterPredicate = this.createFilter();

    if (this.data.message) {
      this.message = this.data.message;

      this.configs.inLoading = true;
      this.messageService.findDetailById(this.message.id)
        .pipe(
          tap((data) => {
            this.message = data;
            this.dataSource.data = data.fields;

            this.formGroup.patchValue({
              ...this.message
            });

          }),
          catchError(this.errorService.handleError),
          finalize(() => {
            this.configs.inLoading = false;
          })
        ).subscribe();

      this.formGroup.patchValue({...this.message});
      this.dataSource.data = this.message.fields;
    }

    if (this.data.view) {
      this.formGroup.get('registryLog').disable();
    }

  }

  private createForm(): void {
    this.formGroup = this.formBuilder.group({
      name: [null, Validators.compose([Validators.required, Validators.minLength(3)])],
      description: [null],
      registryLog: [false]
    });
  }

  private createFilter() {

    const filterFunction = function(data: Field, filter): boolean {

      const searchTerms = JSON.parse(filter);

      return data.name.indexOf(searchTerms.name) !== -1;


    };
    return filterFunction;
  }

  add(): void {
    const dialogRef = this.dialog.open(MessageFieldDetailComponent, {
      data: {title: 'Novo Campo'}
    });

    dialogRef.afterClosed()
      .subscribe(result => {
        if (result.successful) {

          if (this.message.fields) {
            this.message.fields.push(result.data);
            this.dataSource.data = this.message.fields;
          }

          this.snackBar.open(`Campo '${result.data.name}' adicionado com sucesso.`,
            'OK', {verticalPosition: 'top', duration: 5000});
        }
      });
  }

  edit(field: Field): void {

    if (this.data.view) {
      return;
    }

    const idx = this.message.fields.indexOf(field);

    const model = {...field};
    const dialogRef = this.dialog.open(MessageFieldDetailComponent, {
      data: {title: 'Alterar Campo', field: model}
    });

    dialogRef.afterClosed()
      .subscribe(result => {
        if (result.successful) {

          if (this.message.fields) {
            this.message.fields[idx] = result.data;
            this.dataSource.data = this.message.fields;
          }

          this.snackBar.open(`Campo '${result.data.name}' atualizado com sucesso.`,
            'OK', {verticalPosition: 'top', duration: 5000});
        }
      });
  }

  delete(field: Field): void {

    const dialogRef = this.dialog.open<DialogConfirmComponent, DialogConfirmData, boolean>(
      DialogConfirmComponent,
      { data: { title: 'Deletar', message: `Deseja excluir o campo '${field.name}'?` } }
    );

    dialogRef.beforeClosed()
      .pipe(take(1))
      .subscribe((confirmed: boolean) => {
        if (confirmed) {

          if (this.message.fields) {
            this.message.fields.splice(this.message.fields.indexOf(field), 1);
            this.dataSource.data = this.message.fields;

            this.snackBar.open(`Campo excluido com sucesso.`,
              'OK', {verticalPosition: 'top', duration: 5000});
          }

        }
      });

  }

  onSubmit(): void {
    this.configs.inSaving = true;

    const model: Message = {
      ...this.message,
      ...this.formGroup.value
    };

    this.messageService.save(model, model.id)
      .pipe(
        tap((data) => {
          this.configs.inSaving = false;
          this.dialogRef.close({successful: true, message: data});
        }),
        catchError((error) => {
          this.configs.inSaving = false;
          return this.errorService.handleError(error);
        }),
        finalize( () => {
          this.configs.inSaving = false;
          this.configs.inLoading = false;
       })
      ).subscribe();

  }

  onNoClick(): void {
    this.dialogRef.close({});
  }

}


export class MessageDetailData {
  title?: string;
  view?: boolean | false;
  message: Message;
}
