import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import {MessageDetailComponent} from '../message-detail/message-detail.component';
import {DialogConfirmComponent, DialogConfirmData} from '../../../shared/components/dialog-confirm/dialog-confirm.component';
import {MessageService} from '../../services/message.service';
import {ErrorService} from '../../../core/services/error.service';
import {HttpDataSource} from '../../../shared/components/http-data-source/http-data.source';

import {Message} from '../../../core/models/message.model';
import {catchError, finalize, map, take, tap} from 'rxjs/operators';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MessageSearchComponent} from '../message-search/message-search.component';
import {ReplaySubject} from 'rxjs';
import {
  DataFieldLogSearchComponent,
  DataFieldLogSearchData
} from '../../../message-log/components/datafield-log-search/data-field-log-search.component';
import {DataFieldLog} from '../../../core/models/data-field-log';
import {ConditionUtil} from '../../../core/utils/condition-util';
import {MatExpansionPanel} from '@angular/material';

@Component({
  selector: 'app-message-list',
  templateUrl: './message-list.component.html',
  styleUrls: ['./message-list.component.scss']
})
export class MessageListComponent implements OnInit {

  displayedColumns = ['name', 'description', 'actions'];
  dataSource = new HttpDataSource<Message>();
  dataFields: DataFieldLog[] = [];

  selectable = true;
  removable = true;

  configs = {
    resultsLength: 0,
    inLoadingResults: false,
    isRateLimitReached: false,
    pageable: {
      pageIndex: 0,
      pageSize: 20,
      length: 0
    },
    search: {
      done: false,
      message: null,
      selectable: true,
      removable: true
    }
  };

  searchForm: FormGroup;

  private messageSubject = new ReplaySubject<string>(1);

  @ViewChild('filterPanel', { static: true }) filterPanel: MatExpansionPanel;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild('filter', { static: true }) filter: ElementRef;

  constructor(
    private messageService: MessageService,
    private errorService: ErrorService,
    public snackBar: MatSnackBar,
    public dialog: MatDialog,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.configs.inLoadingResults = true;
    this.dataSource.paginator = this.paginator;
    this.createSearchForm();

    this.paginator.page.subscribe((event: PageEvent) => {
      this.load();
    });

    this.load();

    this.dataSource.filterPredicate =
      (data: Message, filter: string) => {
        return (data.description.toLowerCase().indexOf(filter) !== -1);

      };
  }

  private load(search?: boolean): void {

    if (search) {
      this.paginator.pageIndex = 0;
    }

    this.configs.inLoadingResults = true;
    this.messageService.find({...this.searchForm.value})
      .pipe(
        map(res => {
          this.configs.pageable.length = res.total;

          if (res.pageable) {
            this.configs.pageable.pageIndex = res.pageable.page;
          }

          return res.content || [];
        }),
        tap((data) => {
          this.configs.resultsLength = data.length;
          this.dataSource.updateData(data, this.configs.pageable.length);

          if (search && data.length > 0) {
            this.filterPanel.close();
            this.configs.search.done = true;
          }

        }),
        catchError((error) => {
          return this.errorService.handleError(error);
        }),
        finalize(() => {
          this.configs.inLoadingResults = false;
        })
      ).subscribe();

  }

  private createSearchForm(): void {
    this.searchForm = this.formBuilder.group({
      id: [null],
    });
  }

  refresh(): void {
    this.load();
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  add(): void {
    const dialogRef = this.dialog.open(MessageDetailComponent, {
      data: {title: 'Nova Mensagem'}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.successful) {
        this.refresh();
        this.snackBar.open(`Mensagem cadastrada com sucesso.`,
          'OK', {verticalPosition: 'top', duration: 5000});
        this.refresh();
      }
    });

  }

  edit(message: Message): void {
    const s = {...message};
    const dialogRef = this.dialog.open(MessageDetailComponent, {
      data: {title: 'Alterar Mensagem', message: s}
    });

    dialogRef.afterClosed()
      .subscribe(result => {
        if (result.successful) {
          this.snackBar.open(`Mensagem atualizada com sucesso.`,
            'OK', {verticalPosition: 'top', duration: 5000});
          this.refresh();
        }
      });

  }

  delete(message: Message) {
    const dialogRef = this.dialog.open<DialogConfirmComponent, DialogConfirmData, boolean>(
      DialogConfirmComponent,
      { data: { title: 'Deletar', message: `Deseja excluir a mensagem '${message.name}'?` } }
    );

    dialogRef.beforeClosed()
      .pipe(take(1))
      .subscribe((confirmed: boolean) => {
        if (confirmed) {
          this.messageService.delete(message.id)
            .pipe(
              take(1),
              tap(() => {
                this.refresh();
                this.snackBar.open(`Mensagem excluída com sucesso.`,
                  'OK', {verticalPosition: 'top', duration: 5000});
              }),
              catchError((error) => {
                return this.errorService.handleError(error);
              })
            ).subscribe();
        }
      });

  }

  messageSearch(): void {
    const dialogRef = this.dialog.open(MessageSearchComponent);

    dialogRef.beforeClosed()
      .subscribe((result => {
        if (result.data) {
          this.configs.search.message = result.data;
          this.messageSubject.next(result.data.id);
          this.searchForm.patchValue({id: result.data.id});
        }
      }));
  }

  messageReset(): void {
    this.configs.search.message = null;
    this.dataFields = [];
    this.searchForm.patchValue({id: null});
  }

  search(): void {
    this.load(true);
  }

  dataFieldSearch(): void {
    const dialogRef = this.dialog.open<DataFieldLogSearchComponent, DataFieldLogSearchData>(
      DataFieldLogSearchComponent,
      {
        data: {message: this.configs.search.message}
      });

    dialogRef.beforeClosed()
      .subscribe((dataField: DataFieldLog) => {
        if (dataField) {

          const find = this.dataFields.find(data => {
            return (data.fieldId === dataField.fieldId)
              && (data.operator === dataField.operator)
              && (data.value === dataField.value);
          });

          if (find) {
            this.dialog.open<DialogConfirmComponent, DialogConfirmData, boolean>(
              DialogConfirmComponent,
              { data: {
                  title: 'Atenção',
                  message: `Filtro '${this.getExpression(dataField)}' já adicionado.`,
                  cancelButton: false,
                  primaryButton: 'OK'
                } }
            );
          } else {
            this.dataFields.push(dataField);
          }

        }
      });
  }

  editDataFieldSearch(dataFieldLog: DataFieldLog): void {
    const idx = this.dataFields.indexOf(dataFieldLog);
    const dialogRef = this.dialog.open<DataFieldLogSearchComponent, DataFieldLogSearchData>(
      DataFieldLogSearchComponent,
      {
        data: {
          message: this.configs.search.message,
          dataField: dataFieldLog
        }
      });

    dialogRef.beforeClosed()
      .subscribe((dataField: DataFieldLog) => {
        if (dataField) {
          this.dataFields[idx] = dataField;
        }
      });
  }

  removeDataFieldFilter(dataField: DataFieldLog): void {
    this.dataFields.splice(this.dataFields.indexOf(dataField), 1);
  }

  getExpression(dataFieldLog: DataFieldLog): string {
    return ConditionUtil.getDataFieldLogExpression(dataFieldLog);
  }

  reset(): void {
    this.searchForm.reset();
    this.configs.search.message = null;
    this.dataFields = [];
    this.configs.search.done = false;
    this.load();
  }

}
