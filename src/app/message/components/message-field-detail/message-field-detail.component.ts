import {Component, Inject, OnInit, Optional} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import {Field} from '../../../core/models/field.model';

@Component({
  selector: 'app-message-field-detail',
  templateUrl: './message-field-detail.component.html',
  styleUrls: ['./message-field-detail.component.scss']
})
export class MessageFieldDetailComponent implements OnInit {

  field: Field = {};
  formGroup: FormGroup;

  configs = {
    inLoading: false,
    inSaving: false
  };

  constructor(
    @Optional() public dialogRef: MatDialogRef<MessageFieldDetailComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: MessageFieldDetailData,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    if (this.dialogRef) {
      this.dialogRef.disableClose = true;
    }
    this.createForm();

    if (this.data.field) {
      this.field = this.data.field;
      this.formGroup.patchValue({...this.field});
    }

  }

  private createForm(): void {
    this.formGroup = this.formBuilder.group({
      name: ['', Validators.compose([Validators.required, Validators.minLength(3)])],
      description: [null],
      decimalPoint: [false],
      type: ['', Validators.required],
      valueFormat: [null]
    });
  }

  changeType(): void {
    const decimalControl = this.formGroup.get('decimalPoint');
    if (this.formGroup.get('type').value === 'NUMBER') {
       decimalControl.enable();
    } else {
      decimalControl.disable();
      decimalControl.setValue(false);
    }
  }

  get numberType(): boolean {
    if (!this.formGroup) {
      return false;
    }

    if (this.formGroup.get('type').value === 'NUMEBER') {
      return true;
    } else {
      this.formGroup.patchValue({decimalPoint: false});
      return false;
    }
  }

  onOk(): void {
    this.configs.inSaving = true;

    const model: Field = {
      id: this.field.id,
      ...this.formGroup.value
    };

    this.dialogRef.close({successful: true, data: model});

  }

  onNoClick(): void {
    this.dialogRef.close({});
  }

}

export class MessageFieldDetailData {
  title: string;
  field: Field;
}
