import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MessageListComponent} from './components/message-list/message-list.component';
import {MessageDetailComponent} from './components/message-detail/message-detail.component';
import {AuthGuard} from '../login/auth.guard';

const routes: Routes = [
  {
    path: '',
    component: MessageListComponent,
    canActivate: [ AuthGuard ],
    canActivateChild: [ AuthGuard ]
  },
  {
    path: 'new',
    component: MessageDetailComponent,
    canActivate: [ AuthGuard ],
    canActivateChild: [ AuthGuard ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MessageRoutingModule { }
