import { MatTableDataSource } from '@angular/material/table';
export class HttpDataSource<T> extends MatTableDataSource<T> {

  _pageData(data: T[]): T[] {
    if (!this.paginator) { return data; }

    /*const startIndex = this.paginator.pageIndex * this.paginator.pageSize;
    return data.slice().splice(startIndex, this.paginator.pageSize);*/
    return data;
  }

  _filterData(data: T[]) {
    // If there is a filter string, filter out data that does not contain it.
    // Each data object is converted to a string using the function defined by filterTermAccessor.
    // May be overriden for customization.
    this.filteredData =
      !this.filter ? data : data.filter(obj => this.filterPredicate(obj, this.filter));

    // if (this.paginator) { this._updatePaginator(this.filteredData.length); }

    return this.filteredData;
  }

  updateData(pageData: T[], total: number) {
    this.data = pageData;
    this._pageData(pageData);
    this._updatePaginator(total);
  }

}
