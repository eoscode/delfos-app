import {EventType} from '../models/event.model';

export class EventUtil {

  static getTypeDescription(type: EventType | string): string {
    switch (type) {
      case EventType.WARNING:
        return 'Atenção';
      case EventType.INFORMATION:
        return 'Informação';
      case EventType.ERROR:
        return 'Erro';
      case EventType.FATAL:
        return 'Fatal';
      default:
        return '';
    }
  }

  static getShortDescription(type: EventType | string): string {
    switch (type) {
      case EventType.WARNING:
        return 'WARN';
      case EventType.INFORMATION:
        return 'INFO';
      case EventType.ERROR:
        return 'ERR';
      case EventType.FATAL:
        return 'FATAL';
      default:
        return '';
    }
  }

}
