import {environment} from '../../../environments/environment';

export class BaseService {

  getBasePath() {
    return environment.baseUrl;
  }

}
