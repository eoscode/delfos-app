import {Injectable} from '@angular/core';
import {BaseService} from './base.service';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Config} from '../models/config.model';
import {take, tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ConfiguracaoService extends BaseService {

  private _versao?: string;

  constructor(
    private http: HttpClient
  ) {
    super();

    this.loadVersao()
      .pipe(
        take(1),
        tap((data: Config) => {
          this._versao = data.versao;
        })
      ).subscribe();

   }

  private getUrl(): string {
    return `${this.getBasePath()}/config`;
  }

  get versao(): string {
    return this._versao;
  }

  private loadVersao(): Observable<Config> {
    return this.http.get<Config>(`${this.getUrl()}/version`);
  }

}
