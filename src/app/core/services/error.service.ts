import { Injectable, NgZone } from '@angular/core';
import { throwError } from 'rxjs';
import { StandardError } from '../models/standard-error.model';
import { HttpErrorResponse } from '@angular/common/http';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class ErrorService {

  constructor(
    private snackBar: MatSnackBar,
    private ngZone: NgZone
  ) { }

  getErrorMessage(error: any): string {
    return error || 'erro generico';
  }

  public handleError(error: Response | Error | any) {
    let errorMessage: string;

    if (error instanceof HttpErrorResponse) {

      const standardError: StandardError = error.error;
      if (standardError.errors) {
        standardError.errors.forEach((err: any) => {
          this.ngZone.run(() => {
            this.snackBar.open(err.message, 'Ok', {duration: 5000, verticalPosition: 'top'});
          });
        });
      } else {
        errorMessage = `${error.url}: ${error.status} - ${error.statusText || ''}`;
        this.ngZone.run(() => {
          this.snackBar.open(errorMessage, 'Ok', {duration: 5000, verticalPosition: 'top'});
        });
      }

    } else if (error instanceof Error) {
      errorMessage = error.message ? error.message : error.toString();
      this.ngZone.run(() => {
        this.snackBar.open(errorMessage, 'Ok', {duration: 5000, verticalPosition: 'top'});
      });
    } else {
      this.ngZone.run(() => {
        this.snackBar.open(error.toString(), 'Ok', {duration: 5000, verticalPosition: 'top'});
      });
    }

    return throwError(errorMessage);
  }

}
