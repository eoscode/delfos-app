import {Component} from '../component.model';
import {Event, EventType} from '../event.model';
import { Rule } from '../rule.model';
import {Message} from '../message.model';

export interface EventLogStatisticView {

  date: number;
  dateHour: number;
  component: Component;
  identifier: string;
  eventType: EventType | string;
  event: Event;
  rule: Rule;
  message: Message;
  count: number;

}
