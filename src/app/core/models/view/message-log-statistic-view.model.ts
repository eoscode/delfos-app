import {Component} from '../component.model';

export interface MessageLogStatisticView {

  date: number;
  dateHour: number;
  component: Component;
  identifier: string;
  messageId: string;
  message: string;

  count: number;

}
