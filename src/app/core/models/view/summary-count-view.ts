export interface SummaryCountView {

  viewId: string;
  count: number;
  groups: string[];

}
