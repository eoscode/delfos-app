import {DataFieldLog} from './data-field-log';
import {Component} from './component.model';

export interface MessageLog {

  id?: string;
  dateHour?: number;
  messageId?: string;
  message?: string;
  identifier?: string;
  componentId?: string;
  component?: Component;
  hash?: string;

  dataFields?: DataFieldLog[];

}
