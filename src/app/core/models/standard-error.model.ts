export interface StandardError {

  timestamp?: number;
  status?: number;
  errors?: Array<any>[];
  message?: string;
  path?: string;

}
