import {FieldType} from './field-type.model';

export interface DataFieldLog {

  fieldId?: string;
  name?: string;
  type?: FieldType | string;
  value?: any;
  operator?: string;

}
