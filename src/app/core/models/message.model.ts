import {Field} from './field.model';

export interface Message {

  id?: string;
  name?: string;
  description?: string;
  system?: boolean;
  messageGroupId?: string;
  messageGroup?: any;
  fields?: Field[];
  preLoad?: boolean;
  registryLog?: boolean;

}
