import {Condition} from './condition.model';
import {StateType} from './state-type';

export interface Rule {

  id?: string;
  name?: string;
  state?: StateType | string;
  description?: string;
  conditions?: Condition[];
  type?: string;
  limited?: boolean;

}
