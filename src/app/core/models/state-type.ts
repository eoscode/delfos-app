export enum StateType {

  DISABLED = 'DISABLED',
  ENABLED = 'ENABLED',
  DELETED = 'DELETED'

}
