export interface Page {

  content?: any[];
  pageable?: {
    page?: number;
    size?: number;
    sort?: any;
  };
  total?: number;


}
