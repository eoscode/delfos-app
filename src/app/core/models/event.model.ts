import {Rule} from './rule.model';
import {StateType} from './state-type';
import {DataFieldLog} from './data-field-log';

export interface Event {

  id?: string;
  name?: string;
  state?: StateType | string;
  description?: string;
  rules?: Rule[];
  actions?: any[];
  allRules?: boolean;
  delay?: number;
  retry?: number;
  type?: EventType | string;

  dataFields?: DataFieldLog[];

}

export enum EventType {

  OK = 'OK',
  WARNING = 'WARNING',
  INFORMATION = 'INFORMATION',
  ERROR = 'ERROR',
  FATAL = 'FATAL'

}
