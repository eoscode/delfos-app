import {MessageLog} from '../message-log.model';

export interface FilterMessageLogStatistic {

  groupBy?: string[];
  sortBy?: string[];
  filters?: MessageLog;
  limit?: number;

}
