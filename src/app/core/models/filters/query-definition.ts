import {FilterDefinition} from './filter-definition';
import {SortDefinition} from './sort-definition';

export interface QueryDefinition {

  distinct?: boolean;
  filters?: Array<FilterDefinition>;
  sorts?: Array<SortDefinition>;

}
