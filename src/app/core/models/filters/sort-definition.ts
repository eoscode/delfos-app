export interface SortDefinition {

  field: string;
  direction?: Direction;

}

export enum Direction {

  ASC = 'ASC',
  DESC = 'DESC'

}

